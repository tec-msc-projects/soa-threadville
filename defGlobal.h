/******************************************************************************
* ARCHIVO: defGlobal.h
* DESCRIPCIÓN:
* 	Archivo con declaraciones y definiciones para todos los módulos del programa.
*   
* AUTORES:
* 	Mauricio Ramírez
* 	Paula Sánchez
* 	Johan Serrato
******************************************************************************/
#ifndef DEF_GLOBAL_H
	#define DEF_GLOBAL_H

/* CONSTANTES DEL PROGRAMA */

/** Número total de casillas del grafo. */
#define TOTAL_CASILLAS 744

typedef enum {
	NORMAL,
	AUTOPISTA,
	INT_NORMAL,
	INT_ENT_PISTA,
	INT_PUENTE,
	PUENTE
} TipoCasilla;

typedef enum {
	B_ROJO,
	B_VERDE,
	B_AZUL,
	B_BLANCO,
	B_GRIS,
	B_NEGRO,
	B_ROSA,
	B_CELESTE,
	B_NARANJA
} ColorBus;

typedef enum {
	A_ROJO,
	A_AZUL,
	A_VERDE,
	A_NEGRO,
	A_BLANCO,
	A_AMARILLO,
	AMBULANCIA
} ColorAuto;

typedef enum {
	P_LARRY,
	P_CURLY,
	P_MOE,
	P_SHEMP,
	P_JOE
} NombrePuente;

/* Estructuras globales. */
typedef struct
{
	int idCasilla;
	TipoCasilla tipoEspacio;
	int estaOcupada; // 0 ó 1; 1 si lo está; Puede estar ocupada por una reparación.
	char etiquetaPD[8];
	char etiquetaPI[8];
	
	//Solo si tipoEspacio = INT_NORMAL
	int colaInt[4];
	int pFirstOut;
	int pCola;
	
	//Solo si tipoEspacio = INT_PUENTE
	NombrePuente nomPuente;
	int dirEntrada;			// 0 si la entrada es de Norte a Sur; 1 lo contrario
	int semaforoVerde; // 0 ó 1; 1 si está en verde
	int cuentaOfiTran; // Para cuando hay oficial de tránsito. -1 si no hay oficiales.
	int arribo; // Auxiliar para avisar al oficial de tránsito de un arribo al puente.
} Casilla;

/* Variables globales */

// Grafo principal
Casilla threadville[TOTAL_CASILLAS];

// Matrices de Floyd
extern int arcosFloyd[TOTAL_CASILLAS][TOTAL_CASILLAS];
extern int caminosFloyd[TOTAL_CASILLAS][TOTAL_CASILLAS];

// Matriz de paradas
extern int paradas[26][8];

int *obtenerRutaOptima(int , int);

#endif /* DEF_GLOBAL_H */