/******************************************************************************
 * ARCHIVO: gui.c
 * DESCRIPCIÓN:
 * Archivo con los métodos de manejo gráfico de la aplicación.
 *
 * AUTORES:
 * 	Mauricio Ramírez
 * 	Paula Sánchez
 * 	Johan Serrato
 ******************************************************************************/
#include <gtk/gtk.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "../defGlobal.h"
#include "gui.h"
#include "../entidades/entidades.h"

/* CONSTANTES */
#define IMA_DIR "resources/img/"
#define ICONO_P2 "P2.png"
#define SEMA_VERDE "luz_verde.png"
#define SEMA_ROJO "luz_roja.png"

#define ANCHO_DESPLIEGUE 56
#define ALTO_DESPLIEGUE 36

static const char *COLORES_BUSES_G[] = {
	"#ED0E2C", "#4DED0E", "#160EED",
	"#FFFFFF", "#CCCCCC", "#000000",
	"#F598D4", "#BBE1FA", "#FC874C"
};
static const char *COLORES_CARROS_G[] = {
	"rgba(251,5,16,0.2)", "rgba(5,52,251,0.2)", "rgba(49,251,5,0.2)",
	"rgba(0,0,0,0.2)", "rgba(247,248,248,0.3)", "rgba(248,241,5,0.2)",
	"rgba(247,248,248,0.3)"
};
static const char *LABEL_BUSES_G[] = {
	"B.ROJO", "B.VERDE", "B.AZUL",
	"B.BLANCO", "B.GRIS", "B.NEGRO",
	"B.ROSA", "B.CELESTE", "B.NARANJA",
};
static const char *LABEL_CARROS_G[] = {
	"A.ROJO", "A.AZUL", "A.VERDE",
	"A.NEGRO", "A.BLANCO", "A.AMARILLO", "AMBULANCIA"
};
static const char *IMA_CARROS_G[] = {
	"a_rojo.png", "a_azul.png", "a_verde.png",
	"a_negro.png", "a_blanco.png", "a_amarillo.png", "ambu.gif"
};
static const char *IMAGEN_REPARACION[] = {
	"elec.png", "agua.png", "arbol.png",
	"hueco.png", "teleco.png", "repa.png"
};

//static const char *DIRECCIONES[] = {"⇐", "⇑", "⇨", "⇓"};
static const char *DIR_FILES[] = {"dir_izq.png", "dir_arriba.png", "dir_der.png", "dir_abajo.png"};
GdkPixbuf *DIRECCIONES[4];

/* ESTRUCTURAS */
typedef enum {
	IZQUIERDA,
	ARRIBA,
	DERECHA,
	ABAJO,
} DirecCasilla;

typedef struct {
	GtkWidget *window;
	GtkWidget *seccionIzquierda;
	GtkWidget *seccionDerecha;

	GtkWidget *btnSalida;
	GtkWidget *btnGenCarro;
	GtkWidget *btnGenAmbu;
	GtkWidget *btnCrearCarro;
	GtkWidget *btnConfig;
} VentanaBase;

typedef struct {
	GtkListStore *listaParadas;
	GtkWidget *comboTipoAuto;
	GtkWidget *comboParada;
	GtkWidget *listaDialogo;
	GtkWidget *btnDelParada;
	GtkListBoxRow *filaSeleccionada;
} DialogoManual;

typedef struct {
	int debajoPuente; // 1 si es una casilla tapada por un puente; 0 si no
	GtkWidget *contenido;
	GtkWidget *labelParada;
	GtkWidget *labelViajes;
} CasillaG;

typedef struct {
	ColorAuto color;
	ColorBus colorBus;
	int posAnterior;
	int posSiguiente;
	int posColaBus;
	char parada[8];
	int viajes;
} DatosEntidad;

typedef struct {
	int idPuente;
	int estadoN;
	int estadoS;
} DatosPuenteG;

/* VARIABLES GLOBALES */
CasillaG casillasGraficas[TOTAL_CASILLAS];
int pCasillasG = 0;
GtkWidget *contenedorBase; //El grid principal

GdkPixbuf *iconoP2;
GdkPixbuf *imaCarro[7];
GdkPixbuf *semaVerde;
GdkPixbuf *semaRojo;

GdkRGBA colorTransp, colorPasto, colorCalle, colorEtiqueta, colorEtiquetaCarro, colorEtiquetaBus,
				colorCuadra, colorPuente, colorParada, fuenteRepa, fondoRepa, colorActivo, colorDesactivo,
				colorPolicia;

char bufferEtiquetas[8];
PangoFontDescription *fuenteDefecto;
PangoFontDescription *fuenteVehiculo;
PangoFontDescription *fuenteReparacion;

GtkWidget *switchesBus[9];
GtkWidget *switchRepa;
int punterosSwitches[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
DialogoManual controlesDialogo;

/* PROTOTIPOS PRIVADOS */
void switchBus(GtkWidget *, gpointer);
void switchReparaciones(GtkWidget *);
void limpiarGTK(GtkWidget *, VentanaBase *);
GdkPixbuf *create_pixbuf(const gchar *);

gboolean reparacionG_callback(gpointer datosObjeto) {
	DatosEntidad *datosRepa = datosObjeto;
	GdkPixbuf *imaReparacion;
	char bufferTiempo[8];
	if (!casillasGraficas[datosRepa->posAnterior].debajoPuente) {
		imaReparacion = create_pixbuf(IMAGEN_REPARACION[rand() % 6]);
		imaReparacion = gdk_pixbuf_scale_simple(imaReparacion, 20, 20, GDK_INTERP_BILINEAR);
		snprintf(bufferTiempo, 8, "%ds", datosRepa->posSiguiente);
		
		gtk_widget_override_color(casillasGraficas[datosRepa->posAnterior].labelViajes, 0, &fuenteRepa);
		gtk_widget_override_font(casillasGraficas[datosRepa->posAnterior].labelViajes, fuenteReparacion);
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosRepa->posAnterior].labelViajes), bufferTiempo);
		gtk_image_set_from_pixbuf(GTK_IMAGE(casillasGraficas[datosRepa->posAnterior].contenido), imaReparacion);
		gtk_widget_override_background_color(casillasGraficas[datosRepa->posAnterior].contenido, GTK_STATE_NORMAL, &fondoRepa);
	}
	
	datosRepa = NULL;
	g_free(datosObjeto);
	return G_SOURCE_REMOVE;
}
/**
 * Pinta una casilla con una reparación y tiempo restante.
 * @param casilla
 * @param tRestante
 */
void reparacionG(int casilla, int tRestante) {
	DatosEntidad *datosRepa = g_malloc(sizeof(DatosEntidad));
	datosRepa->posAnterior = casilla;
	datosRepa->posSiguiente = tRestante;
	gdk_threads_add_idle(reparacionG_callback, datosRepa);
}

gboolean quitarReparacionG_callback(gpointer datosObjeto) {
	DatosEntidad *datosRepa = datosObjeto;
	if (!casillasGraficas[datosRepa->posAnterior].debajoPuente) {
		gtk_widget_override_color(casillasGraficas[datosRepa->posAnterior].labelViajes, 0, &colorEtiqueta);
		gtk_widget_override_font(casillasGraficas[datosRepa->posAnterior].labelViajes, fuenteVehiculo);
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosRepa->posAnterior].labelViajes), "");
		gtk_image_clear(GTK_IMAGE(casillasGraficas[datosRepa->posAnterior].contenido));
		gtk_widget_override_background_color(casillasGraficas[datosRepa->posAnterior].contenido, GTK_STATE_NORMAL, &colorTransp);
	}
	datosRepa = NULL;
	g_free(datosObjeto);
	return G_SOURCE_REMOVE;
}

/**
 * Quita la represrentación gráfica de una reparación.
 * @param casilla
 */
void quitarReparacionG(int casilla) {
	DatosEntidad *datosRepa = g_malloc(sizeof(DatosEntidad));
	datosRepa->posAnterior = casilla;
	gdk_threads_add_idle(quitarReparacionG_callback, datosRepa);
}

/**
 * Llama a un vehículo para que entre a la ciudad.
 * @param widget
 * @param vBase
 */
void llamarCarro(GtkWidget *widget) {
	generarVehiculo(0);
}

void llamarAmbulancia(GtkWidget *widget) {
	generarVehiculo(1);
}

void enviarDatosCarro() {
	GtkWidget *labelTemp;
	GtkWidget *filaListBox;
	GList *childRow;
	GList *rows = gtk_container_get_children(GTK_CONTAINER(controlesDialogo.listaDialogo));
	int numeroParadas = g_list_length(rows) + 2;
	
	char **listaParadas = malloc(numeroParadas * sizeof(char*));
	int i;
	for (i = 0; i < numeroParadas; i++) {
    listaParadas[i] = malloc((8 + 1) * sizeof(char));
	}
	
	i--;
	snprintf(listaParadas[i--], 9, "%s", "Z");
	while(rows) {
		filaListBox = GTK_WIDGET(rows->data);
		childRow = gtk_container_get_children(GTK_CONTAINER(filaListBox));
		labelTemp = childRow->data;
		g_list_free(childRow);
		snprintf(listaParadas[i--], 9, "%s", gtk_label_get_text(GTK_LABEL(labelTemp)));
		rows = rows->next;
	}
	snprintf(listaParadas[i], 9, "%s", "Y");
	g_list_free(rows);
	
	int tipoCarro = gtk_combo_box_get_active(GTK_COMBO_BOX(controlesDialogo.comboTipoAuto));
	int esAmbulancia = (tipoCarro == 6) ? 1 : 0;
	
	crearVehiculo(tipoCarro, esAmbulancia, listaParadas, numeroParadas);
}

void accionAgregarP(GtkWidget *widget) {
	GtkTreeIter iter;
	gchar *stringParada = NULL;
	
	if(gtk_combo_box_get_active_iter(GTK_COMBO_BOX(controlesDialogo.comboParada), &iter))
	{
		GtkTreeModel *model = gtk_combo_box_get_model(GTK_COMBO_BOX(controlesDialogo.comboParada));
		gtk_tree_model_get(model, &iter, 0, &stringParada, -1 );
	}
	
	int indexInsert = (controlesDialogo.filaSeleccionada != NULL) ? 
		gtk_list_box_row_get_index(controlesDialogo.filaSeleccionada) : 0;
	GtkWidget *labelParada = gtk_label_new(stringParada);
	gtk_list_box_insert(GTK_LIST_BOX(controlesDialogo.listaDialogo), labelParada, indexInsert);
	gtk_widget_show(labelParada);
	
	if(stringParada) {
		g_free(stringParada);
	}
}

void accionFilaSelec(GtkListBox *list_box, GtkListBoxRow *row) {
	controlesDialogo.filaSeleccionada = row;
	int actBtn = (row == NULL) ? FALSE : TRUE;
	gtk_widget_set_sensitive(controlesDialogo.btnDelParada, actBtn);
}

void accionRemoverP(GtkWidget *widget) {
	gtk_widget_set_sensitive(controlesDialogo.btnDelParada, FALSE);
	gtk_widget_destroy(GTK_WIDGET(controlesDialogo.filaSeleccionada));
	controlesDialogo.filaSeleccionada = NULL;
}

/**
 * Abre un diálogo para crear manualmente un carro.
 * @param widget
 * @param vBase
 */
void crearCarro(GtkWidget *widget) {
	GtkWidget *dialogoCreacion = gtk_dialog_new_with_buttons(
		"Creación de auto",
		NULL,
		GTK_DIALOG_MODAL,
		"OK", GTK_RESPONSE_ACCEPT,
		"Cancelar", GTK_RESPONSE_REJECT,
		NULL
	);
	gtk_container_set_border_width(GTK_CONTAINER(dialogoCreacion), 10);
	GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialogoCreacion));
	GtkWidget *gridDiag = gtk_grid_new();
	gtk_grid_set_column_spacing(GTK_GRID(gridDiag), 8);
	gtk_grid_set_row_spacing(GTK_GRID(gridDiag), 8);
	gtk_grid_set_column_homogeneous(GTK_GRID(gridDiag), 1);
	gtk_grid_set_row_homogeneous(GTK_GRID(gridDiag), 1);
	gtk_container_add(GTK_CONTAINER(content_area), gridDiag);
	
	GtkWidget *labelDialog = gtk_label_new("Tipo auto");
	gtk_grid_attach(GTK_GRID(gridDiag), labelDialog, 0, 0, 1, 1);
	
	controlesDialogo.comboTipoAuto = gtk_combo_box_text_new();
	int i;
	for(i = 0; i < 7; i++) {
		gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(controlesDialogo.comboTipoAuto), NULL, LABEL_CARROS_G[i]);
	}
	gtk_grid_attach(GTK_GRID(gridDiag), controlesDialogo.comboTipoAuto, 1, 0, 2, 1);
	gtk_combo_box_set_active(GTK_COMBO_BOX(controlesDialogo.comboTipoAuto), 0);
	
	labelDialog = gtk_label_new("Visita a agregar");
	gtk_grid_attach(GTK_GRID(gridDiag), labelDialog, 0, 1, 1, 1);
	
	controlesDialogo.comboParada = gtk_combo_box_new_with_model(GTK_TREE_MODEL(controlesDialogo.listaParadas));
	GtkCellRenderer *cRender = gtk_cell_renderer_text_new();
	gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(controlesDialogo.comboParada), cRender, TRUE);
	gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(controlesDialogo.comboParada), cRender,
		"text", 0,
		NULL);
	gtk_grid_attach(GTK_GRID(gridDiag), controlesDialogo.comboParada, 1, 1, 1, 1);
	gtk_combo_box_set_active(GTK_COMBO_BOX(controlesDialogo.comboParada), 0);
	
	GtkWidget *btnAParada = gtk_button_new_with_label("+");
	g_signal_connect(btnAParada, "clicked", G_CALLBACK(accionAgregarP), NULL);
	gtk_grid_attach(GTK_GRID(gridDiag), btnAParada, 2, 1, 1, 1);
	
	labelDialog = gtk_label_new("-> Z");
	gtk_widget_set_halign(labelDialog, GTK_ALIGN_START);
	gtk_widget_set_valign(labelDialog, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(gridDiag), labelDialog, 2, 2, 1, 1);
	
	labelDialog = gtk_label_new("Última visita ->");
	gtk_widget_override_font(labelDialog, fuenteDefecto);
	gtk_widget_set_halign(labelDialog, GTK_ALIGN_END);
	gtk_widget_set_valign(labelDialog, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(gridDiag), labelDialog, 0, 2, 1, 1);
	
	GtkWidget *scrollW = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scrollW),
																	GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_widget_override_background_color(scrollW, GTK_STATE_NORMAL, &colorPasto);
	gtk_grid_attach(GTK_GRID(gridDiag), scrollW, 1, 2, 1, 4);
	
	controlesDialogo.listaDialogo = gtk_list_box_new();
	g_signal_connect(controlesDialogo.listaDialogo, "row-selected", G_CALLBACK(accionFilaSelec), NULL);
	gtk_container_add(GTK_CONTAINER(scrollW), controlesDialogo.listaDialogo);
	
	labelDialog = gtk_label_new("Y ->");
	gtk_widget_set_halign(labelDialog, GTK_ALIGN_END);
	gtk_widget_set_valign(labelDialog, GTK_ALIGN_CENTER);
	gtk_grid_attach(GTK_GRID(gridDiag), labelDialog, 0, 5, 1, 1);
	
	controlesDialogo.btnDelParada = gtk_button_new_with_label("Remover");
	gtk_widget_set_sensitive(controlesDialogo.btnDelParada, FALSE);
	g_signal_connect(controlesDialogo.btnDelParada, "clicked", G_CALLBACK(accionRemoverP), NULL);
	gtk_grid_attach(GTK_GRID(gridDiag), controlesDialogo.btnDelParada, 2, 4, 1, 1);
	
	gtk_widget_show_all(dialogoCreacion);
	gint result = gtk_dialog_run(GTK_DIALOG(dialogoCreacion));
	switch(result)
	{
		case GTK_RESPONSE_ACCEPT:
			enviarDatosCarro();
			break;
  }
	controlesDialogo.comboTipoAuto = NULL;
	gtk_widget_destroy(dialogoCreacion);
}

/**
 * Abre un diálogo para crear manualmente un carro.
 * @param widget
 * @param vBase
 */
void dialogoConfig(GtkWidget *widget) {
	GtkWidget *frame;
	GtkWidget *gridCont;
	GtkWidget *verticalBox;
	GtkWidget *label;
	
	GtkWidget *spinK;
	GtkWidget *spinM;
	GtkWidget *spinN;
	GtkWidget *spinRepa;
	GtkWidget *spinDestinos;
	GtkWidget *checkFijo;
	GtkWidget *spinSesgo;
	
	ParamThreadville *valores;
	
	GtkWidget *dialogoParam = gtk_dialog_new_with_buttons(
		"Cambiar parámetros",
		NULL,
		GTK_DIALOG_MODAL,
		"OK", GTK_RESPONSE_ACCEPT,
		"Cancelar", GTK_RESPONSE_REJECT,
		NULL
	);
	gtk_container_set_border_width(GTK_CONTAINER(dialogoParam), 10);
	GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialogoParam));
	verticalBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	gtk_container_add(GTK_CONTAINER(content_area), verticalBox);
	
	// Frame de parámetros para puentes -- -- --
	frame = gtk_frame_new("Puentes");
	gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
	gtk_box_pack_start(GTK_BOX(verticalBox), frame, FALSE, FALSE, 5);
	gridCont = gtk_grid_new();
	gtk_container_set_border_width(GTK_CONTAINER(gridCont), 5);
	gtk_grid_set_column_spacing(GTK_GRID(gridCont), 8);
	gtk_grid_set_row_spacing(GTK_GRID(gridCont), 8);
	gtk_grid_set_column_homogeneous(GTK_GRID(gridCont), 1);
	gtk_grid_set_row_homogeneous(GTK_GRID(gridCont), 1);
	gtk_container_add(GTK_CONTAINER(frame), gridCont);
	
	label = gtk_label_new("Cantidad de autos (K):");
	gtk_grid_attach(GTK_GRID(gridCont), label, 0, 0, 1, 1);
	
	spinK = gtk_spin_button_new_with_range(1, 99, 1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinK), kLarryJoe);
	gtk_grid_attach(GTK_GRID(gridCont), spinK, 1, 0, 1, 1);
	
	label = gtk_label_new("Tiempo semáforo Curly (M):");
	gtk_grid_attach(GTK_GRID(gridCont), label, 0, 1, 1, 1);
	
	spinM = gtk_spin_button_new_with_range(1, 99, 1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinM), mCurly);
	gtk_grid_attach(GTK_GRID(gridCont), spinM, 1, 1, 1, 1);

	label = gtk_label_new("Tiempo semáforo Shemp (N):");
	gtk_grid_attach(GTK_GRID(gridCont), label, 0, 2, 1, 1);

	spinN = gtk_spin_button_new_with_range(1, 99, 1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinN), nShemp);
	gtk_grid_attach(GTK_GRID(gridCont), spinN, 1, 2, 1, 1);
	
	// Frame de parámetros para reparaciones -- -- --
	frame = gtk_frame_new("Reparaciones");
	gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
	gtk_box_pack_start(GTK_BOX(verticalBox), frame, FALSE, FALSE, 5);
	gridCont = gtk_grid_new();
	gtk_container_set_border_width(GTK_CONTAINER(gridCont), 5);
	gtk_grid_set_column_spacing(GTK_GRID(gridCont), 8);
	gtk_grid_set_row_spacing(GTK_GRID(gridCont), 8);
	gtk_grid_set_column_homogeneous(GTK_GRID(gridCont), 1);
	gtk_grid_set_row_homogeneous(GTK_GRID(gridCont), 1);
	gtk_container_add(GTK_CONTAINER(frame), gridCont);
	
	label = gtk_label_new("Tiempo medio (s):");
	gtk_grid_attach(GTK_GRID(gridCont), label, 0, 0, 1, 1);
	
	spinRepa = gtk_spin_button_new_with_range(1, 60, 1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinRepa), mediaReparaciones);
	gtk_grid_attach(GTK_GRID(gridCont), spinRepa, 1, 0, 1, 1);
	
	// Frame de parámetros para generación de carros -- -- --
	frame = gtk_frame_new("Generación de carros");
	gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
	gtk_box_pack_start(GTK_BOX(verticalBox), frame, FALSE, FALSE, 5);
	gridCont = gtk_grid_new();
	gtk_container_set_border_width(GTK_CONTAINER(gridCont), 5);
	gtk_grid_set_column_spacing(GTK_GRID(gridCont), 8);
	gtk_grid_set_row_spacing(GTK_GRID(gridCont), 8);
	gtk_grid_set_column_homogeneous(GTK_GRID(gridCont), 1);
	gtk_grid_set_row_homogeneous(GTK_GRID(gridCont), 1);
	gtk_container_add(GTK_CONTAINER(frame), gridCont);
	
	label = gtk_label_new("Máximo destinos:");
	gtk_grid_attach(GTK_GRID(gridCont), label, 0, 0, 1, 1);
	
	spinDestinos = gtk_spin_button_new_with_range(0, 200, 1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinDestinos), maxDestinos);
	gtk_grid_attach(GTK_GRID(gridCont), spinDestinos, 1, 0, 1, 1);
	
	checkFijo = gtk_check_button_new_with_label("Cantidad fija");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkFijo), cantidadFija);
	gtk_widget_set_halign(checkFijo, GTK_ALIGN_END);
	gtk_widget_set_valign(checkFijo, GTK_ALIGN_START);
	gtk_grid_attach(GTK_GRID(gridCont), checkFijo, 1, 1, 1, 1);
	
	label = gtk_label_new("Sesgo visitas a Y y Z:");
	gtk_grid_attach(GTK_GRID(gridCont), label, 0, 2, 1, 1);
	
	spinSesgo = gtk_spin_button_new_with_range(1, 2, 0.05);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spinSesgo), sesgoRotondas);
	gtk_grid_attach(GTK_GRID(gridCont), spinSesgo, 1, 2, 1, 1);
	
	gtk_widget_show_all(dialogoParam);
	gint result = gtk_dialog_run(GTK_DIALOG(dialogoParam));
	switch(result)
	{
		case GTK_RESPONSE_ACCEPT:
			valores = malloc(sizeof(ParamThreadville));
			valores->k = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinK));
			valores->m = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinM));
			valores->n = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinN));
			valores->fRepa = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinRepa));
			valores->maxDest = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinDestinos));
			valores->fixedDest = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkFijo));
			valores->sesgoRot = gtk_spin_button_get_value(GTK_SPIN_BUTTON(spinSesgo));
			actualizarParametros(valores);
			break;
  }
	gtk_widget_destroy(dialogoParam);
}

gboolean avanzarBusG_callback(gpointer datosObjeto) {
	DatosEntidad *datosBus = datosObjeto;
	GdkRGBA colorBus;
	gdk_rgba_parse(&colorBus, COLORES_BUSES_G[datosBus->colorBus]);

	if (!casillasGraficas[datosBus->posSiguiente].debajoPuente) {
		gtk_widget_override_color(casillasGraficas[datosBus->posSiguiente].labelParada, 0, &colorEtiquetaBus);
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosBus->posSiguiente].labelParada), datosBus->parada);
		gtk_widget_override_background_color(casillasGraficas[datosBus->posSiguiente].contenido, GTK_STATE_NORMAL, &colorBus);
	}
	if (!casillasGraficas[datosBus->posAnterior].debajoPuente) {
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosBus->posAnterior].labelParada), "");
		if (datosBus->posAnterior == datosBus->posColaBus) {
			gtk_widget_override_background_color(casillasGraficas[datosBus->posAnterior].contenido, GTK_STATE_NORMAL, &colorBus);
		}
	}
	if (!casillasGraficas[datosBus->posColaBus].debajoPuente && (datosBus->posAnterior != datosBus->posColaBus)) {
		gtk_widget_override_background_color(casillasGraficas[datosBus->posColaBus].contenido, GTK_STATE_NORMAL, &colorTransp);
	}
	datosBus = NULL;
	g_free(datosObjeto);
	return G_SOURCE_REMOVE;
}

/**
 * Mueve un autobus en la ciudad.
 * @param color
 * @param posAnterior
 * @param posSiguiente
 * @param posCola
 * @param parada
 */
void avanzarBusG(ColorBus color, int posAnterior, int posSiguiente, int posCola, const char *parada) {
	DatosEntidad *datosBus = g_malloc(sizeof(DatosEntidad));
	datosBus->colorBus = color;
	datosBus->posAnterior = posAnterior;
	datosBus->posSiguiente = posSiguiente;
	datosBus->posColaBus = posCola;
	snprintf(datosBus->parada, 8, "%s", parada);
	gdk_threads_add_idle(avanzarBusG_callback, datosBus);
}

gboolean matarBusG_callback(gpointer datosObjeto) {
	DatosEntidad *datosBus = datosObjeto;
	if (!casillasGraficas[datosBus->posAnterior].debajoPuente) {
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosBus->posAnterior].labelParada), "");
		gtk_widget_override_background_color(casillasGraficas[datosBus->posAnterior].contenido, GTK_STATE_NORMAL, &colorTransp);
	}
	if (!casillasGraficas[datosBus->posColaBus].debajoPuente) {
		gtk_widget_override_background_color(casillasGraficas[datosBus->posColaBus].contenido, GTK_STATE_NORMAL, &colorTransp);
	}
	datosBus = NULL;
	g_free(datosObjeto);
	return G_SOURCE_REMOVE;
}

/**
 * Limpia de la gráfica a un bus.
 * @param posActual
 */
void matarBusG(int posActual, int posCola) {
	DatosEntidad *datosBus = g_malloc(sizeof(DatosEntidad));
	datosBus->posAnterior = posActual;
	datosBus->posColaBus = posCola;
	gdk_threads_add_idle(matarBusG_callback, datosBus);
}

gboolean avanzarAutomovilG_callback(gpointer datosObjeto) {
	DatosEntidad *datosAuto = datosObjeto;
	char bufferV[8];
	GdkRGBA colorAuto;
	gdk_rgba_parse(&colorAuto, COLORES_CARROS_G[datosAuto->color]);

	if (!casillasGraficas[datosAuto->posSiguiente].debajoPuente) {
		snprintf(bufferV, 8, "%d", datosAuto->viajes);
		gtk_widget_override_color(casillasGraficas[datosAuto->posSiguiente].labelParada, 0, &colorEtiquetaCarro);
		gtk_widget_override_color(casillasGraficas[datosAuto->posSiguiente].labelViajes, 0, &colorEtiquetaCarro);
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosAuto->posSiguiente].labelParada), datosAuto->parada);
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosAuto->posSiguiente].labelViajes), bufferV);
		gtk_widget_override_background_color(casillasGraficas[datosAuto->posSiguiente].contenido, GTK_STATE_NORMAL, &colorAuto);
		gtk_image_set_from_pixbuf(GTK_IMAGE(casillasGraficas[datosAuto->posSiguiente].contenido), imaCarro[datosAuto->color]);
	}
	if (!casillasGraficas[datosAuto->posAnterior].debajoPuente
					&& ((datosAuto->posAnterior - datosAuto->posSiguiente) != 0)) {
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosAuto->posAnterior].labelParada), "");
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosAuto->posAnterior].labelViajes), "");
		gtk_widget_override_background_color(casillasGraficas[datosAuto->posAnterior].contenido, GTK_STATE_NORMAL, &colorTransp);
		gtk_image_clear(GTK_IMAGE(casillasGraficas[datosAuto->posAnterior].contenido));
	}
	datosAuto = NULL;
	g_free(datosObjeto);
	return G_SOURCE_REMOVE;
}

/**
 * Actualiza el avance de un automóvil en el gráfico.
 * @param color
 * @param posAnterior
 * @param posSiguiente
 * @param parada
 * @param viajes
 */
void avanzarAutomovilG(ColorAuto color, int posAnterior, int posSiguiente, const char *parada,
				int viajes) {
	DatosEntidad *datosAuto = g_malloc(sizeof(DatosEntidad));
	datosAuto->color = color;
	datosAuto->posAnterior = posAnterior;
	datosAuto->posSiguiente = posSiguiente;
	snprintf(datosAuto->parada, 8, "%s", parada);
	datosAuto->viajes = viajes;
	gdk_threads_add_idle(avanzarAutomovilG_callback, datosAuto);
}

gboolean matarAutomovilG_callback(gpointer datosObjeto) {
	DatosEntidad *datosAuto = datosObjeto;
	if (!casillasGraficas[datosAuto->posAnterior].debajoPuente) {
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosAuto->posAnterior].labelParada), "");
		gtk_label_set_text(GTK_LABEL(casillasGraficas[datosAuto->posAnterior].labelViajes), "");
		gtk_widget_override_background_color(casillasGraficas[datosAuto->posAnterior].contenido, GTK_STATE_NORMAL, &colorTransp);
		gtk_image_clear(GTK_IMAGE(casillasGraficas[datosAuto->posAnterior].contenido));
	}
	datosAuto = NULL;
	g_free(datosObjeto);
	return G_SOURCE_REMOVE;
}

/**
 * Limpia de la gráfica a un automóvil.
 * @param posActual
 */
void matarAutomovilG(int posActual) {
	DatosEntidad *datosAuto = g_malloc(sizeof(DatosEntidad));
	datosAuto->posAnterior = posActual;
	gdk_threads_add_idle(matarAutomovilG_callback, datosAuto);
}

/**
 * Crea un buffer para cargar una imagen.
 * @param filename
 * @return
 */
GdkPixbuf *create_pixbuf(const gchar * filename) {
	GdkPixbuf *pixbuf;
	GError *error = NULL;
	char bufferRuta[256];
	strcpy(bufferRuta, IMA_DIR);
	strcat(bufferRuta, filename);
	pixbuf = gdk_pixbuf_new_from_file(bufferRuta, &error);
	if (!pixbuf) {
		fprintf(stderr, "%s\n", error->message);
		g_error_free(error);
	}
	return pixbuf;
}

/**
 * Añade una casilla al gráfico y la mapea para ser accedida por parte de la lógica del programa.
 * @param posH
 * @param posV
 */
void pavimentarCasilla(int posH, int posV, DirecCasilla marca) {
	GtkWidget *overlayC = gtk_grid_get_child_at(GTK_GRID(contenedorBase), posH, posV);
	GList *children = gtk_container_get_children(GTK_CONTAINER(overlayC));
	GtkWidget *fondo = children->data;
	g_list_free(children);

	if (marca != -1) {
		//GtkWidget *flecha = gtk_label_new(DIRECCIONES[marca]);
		GtkWidget *flecha = gtk_image_new_from_pixbuf(DIRECCIONES[marca]);
		gtk_overlay_add_overlay(GTK_OVERLAY(overlayC), flecha);
	}
	
	gtk_widget_override_background_color(fondo, GTK_STATE_NORMAL, &colorCalle);
	casillasGraficas[pCasillasG].contenido = gtk_image_new();
	gtk_overlay_add_overlay(GTK_OVERLAY(overlayC), casillasGraficas[pCasillasG].contenido);
	
	casillasGraficas[pCasillasG].debajoPuente = 0;
	
	casillasGraficas[pCasillasG].labelParada = gtk_label_new("");
	gtk_widget_set_halign(casillasGraficas[pCasillasG].labelParada, GTK_ALIGN_END);
	gtk_widget_set_valign(casillasGraficas[pCasillasG].labelParada, GTK_ALIGN_START);
	gtk_widget_override_font(casillasGraficas[pCasillasG].labelParada, fuenteVehiculo);
	gtk_widget_override_color(casillasGraficas[pCasillasG].labelParada, 0, &colorEtiqueta);
	gtk_overlay_add_overlay(GTK_OVERLAY(overlayC), casillasGraficas[pCasillasG].labelParada);
	
	casillasGraficas[pCasillasG].labelViajes = gtk_label_new("");
	gtk_widget_set_halign(casillasGraficas[pCasillasG].labelViajes, GTK_ALIGN_START);
	gtk_widget_set_valign(casillasGraficas[pCasillasG].labelViajes, GTK_ALIGN_END);
	gtk_widget_override_font(casillasGraficas[pCasillasG].labelViajes, fuenteVehiculo);
	gtk_widget_override_color(casillasGraficas[pCasillasG].labelViajes, 0, &colorEtiqueta);
	gtk_overlay_add_overlay(GTK_OVERLAY(overlayC), casillasGraficas[pCasillasG].labelViajes);
	/*
	GtkWidget *labelCasilla;
	snprintf(bufferEtiquetas, 8, "%d", pCasillasG);
	labelCasilla = gtk_label_new(bufferEtiquetas);
	gtk_widget_override_font(labelCasilla, fuenteDefecto);
	gtk_widget_set_halign(labelCasilla, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(labelCasilla, GTK_ALIGN_CENTER);
	gtk_widget_override_color(labelCasilla, 0, &colorEtiqueta);
	gtk_overlay_add_overlay(GTK_OVERLAY(overlayC), labelCasilla);
	*/
	pCasillasG++;
}

/**
 * Crea los componentes para el contenedor derecho de la pantalla.
 * @param vBase
 */
void montarContenedorDerecha(VentanaBase *vBase) {
	vBase->seccionDerecha = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

	/* Botón para salir de la aplicación GTK */
	vBase->btnSalida = gtk_button_new_with_label("Salir");
	g_signal_connect(vBase->btnSalida, "clicked", G_CALLBACK(limpiarGTK), &vBase);
	gtk_box_pack_start(GTK_BOX(vBase->seccionDerecha), vBase->btnSalida, 0, 0, 0);

	/* Botón para generar un vehículo. */
	vBase->btnGenCarro = gtk_button_new_with_label("Generar\ncarro");
	g_signal_connect(vBase->btnGenCarro, "clicked", G_CALLBACK(llamarCarro), NULL);
	gtk_box_pack_start(GTK_BOX(vBase->seccionDerecha), vBase->btnGenCarro, 0, 0, 0);
	
	/* Botón para generar una ambulancia. */
	vBase->btnGenAmbu = gtk_button_new_with_label("Generar\nambulancia");
	g_signal_connect(vBase->btnGenAmbu, "clicked", G_CALLBACK(llamarAmbulancia), NULL);
	gtk_box_pack_start(GTK_BOX(vBase->seccionDerecha), vBase->btnGenAmbu, 0, 0, 0);
	
	/* Botón para crear un vehículo manualmente. */
	vBase->btnCrearCarro = gtk_button_new_with_label("Crear\ncarro...");
	g_signal_connect(vBase->btnCrearCarro, "clicked", G_CALLBACK(crearCarro), NULL);
	gtk_box_pack_start(GTK_BOX(vBase->seccionDerecha), vBase->btnCrearCarro, 0, 0, 0);
	
	/* Switch para activar y desactivar las reparaciones. */
	switchRepa = gtk_toggle_button_new_with_label("Reparaciones");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(switchRepa), TRUE);
	gtk_widget_override_font(switchRepa, fuenteDefecto);
	gtk_widget_override_color(switchRepa, GTK_STATE_NORMAL, &colorActivo);
	g_signal_connect(switchRepa, "toggled", G_CALLBACK(switchReparaciones), NULL);
	gtk_box_pack_start(GTK_BOX(vBase->seccionDerecha), switchRepa, 0, 0, 1);
	
	/* Botón de configuración de parámetros. */
	vBase->btnConfig = gtk_button_new_with_label("Parámetros...");
	gtk_widget_override_font(vBase->btnConfig, fuenteDefecto);
	g_signal_connect(vBase->btnConfig, "clicked", G_CALLBACK(dialogoConfig), NULL);
	gtk_box_pack_start(GTK_BOX(vBase->seccionDerecha), vBase->btnConfig, 0, 0, 1);
}

/**
 * Crea los componentes para el contenedor de la izquierda de la pantalla.
 * @param vBase
 */
void montarContenedorIzquierda(VentanaBase *vBase) {
	GtkWidget *imagenP2;
	GdkRGBA colorBus;

	vBase->seccionIzquierda = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

	iconoP2 = gdk_pixbuf_scale_simple(iconoP2, 75, 75, GDK_INTERP_BILINEAR);
	imagenP2 = gtk_image_new_from_pixbuf(iconoP2);
	gtk_box_pack_start(GTK_BOX(vBase->seccionIzquierda), imagenP2, 0, 0, 0);
	
	int b;
	for (b = B_ROJO; b <= B_NARANJA; b++) {
		gdk_rgba_parse(&colorBus, COLORES_BUSES_G[b]);
		switchesBus[b] = gtk_toggle_button_new_with_label(LABEL_BUSES_G[b]);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(switchesBus[b]), TRUE);
		gtk_widget_override_font(switchesBus[b], fuenteDefecto);
		gtk_widget_override_color(switchesBus[b], GTK_STATE_NORMAL, &colorBus);
		g_signal_connect(switchesBus[b], "toggled", G_CALLBACK(switchBus), &punterosSwitches[b]);
		gtk_box_pack_start(GTK_BOX(vBase->seccionIzquierda), switchesBus[b], 0, 0, 0);
	}
}

/**
 * Construye la autopista gráfica.
 */
void construyeAutopistaG() {
	printf("\tConstruyendo autopista y rotondas...\n");
	int trazoRotondaYH[] = {0, 0, 0, 1, 1, 2, 3, 4, 4, 5, 5, 5, 5, 5, 5, 4, 4, 3, 2, 1, 1, 0, 0, 0};
	int trazoRotondaYV[] = {18, 19, 20, 20, 21, 21, 21, 21, 20, 20, 19, 18, 17, 16, 15, 15, 14, 14, 14, 14, 15, 15, 16, 17};
	int trazoRotondaZH[] = {55, 55, 54, 54, 53, 52, 51, 51, 50, 50, 50, 50, 50, 50, 51, 51, 52, 53, 54, 54, 55, 55, 55, 55};
	int trazoRotondaZV[] = {16, 15, 15, 14, 14, 14, 14, 15, 15, 16, 17, 18, 19, 20, 20, 21, 21, 21, 21, 20, 20, 19, 18, 17};

	int pTrazo;
	// Rotonda Y
	for (pTrazo = 0; pTrazo < 24; pTrazo++) {
		pavimentarCasilla(trazoRotondaYH[pTrazo], trazoRotondaYV[pTrazo], -1);
	}

	// Autopista carril sur
	int alturaPista = 20;
	int direccion = DERECHA;
	for (pTrazo = 6; pTrazo <= 49; pTrazo++) {
		if (pTrazo == 12 || pTrazo == 20 || pTrazo == 28 || pTrazo == 36 || pTrazo == 44) {
			casillasGraficas[pCasillasG++].debajoPuente = 1;
			casillasGraficas[pCasillasG++].debajoPuente = 1;
		}
		else {
			pavimentarCasilla(pTrazo, alturaPista, direccion);
			pavimentarCasilla(pTrazo, alturaPista - 1, direccion);
		}
		direccion = -1;
	}

	// Rotonda Z
	for (pTrazo = 0; pTrazo < 24; pTrazo++) {
		pavimentarCasilla(trazoRotondaZH[pTrazo], trazoRotondaZV[pTrazo], -1);
	}

	// Autopista carril norte
	alturaPista = 15;
	direccion = IZQUIERDA;
	for (pTrazo = 49; pTrazo >= 6; pTrazo--) {
		if (pTrazo == 12 || pTrazo == 20 || pTrazo == 28 || pTrazo == 36 || pTrazo == 44) {
			casillasGraficas[pCasillasG++].debajoPuente = 1;
			casillasGraficas[pCasillasG++].debajoPuente = 1;
		}
		else {
			pavimentarCasilla(pTrazo, alturaPista, direccion);
			pavimentarCasilla(pTrazo, alturaPista + 1, direccion);
		}
		direccion = -1;
	}
}

/**
 * Construye la calle de circunvalación gráfica.
 */
void construyeCircunvalacionG() {
	printf("\tConstruyendo la circunvalación...\n");
	int trazoH, trazoV;
	int direccion = ARRIBA;
	for (trazoH = 4, trazoV = 13; trazoV > 0; trazoV--) {
		pavimentarCasilla(trazoH, trazoV, direccion);
		direccion = -1;
	}
	direccion = DERECHA;
	for (; trazoH < 51; trazoH++) {
		pavimentarCasilla(trazoH, trazoV, direccion);
		direccion = -1;
	}
	direccion = ABAJO;
	for (; trazoV < 35; trazoV++) {
		pavimentarCasilla(trazoH, trazoV, direccion);
		direccion = -1;
		if (trazoV == 13) {
			trazoV = 21;
			direccion = ABAJO;
		}
	}
	direccion = IZQUIERDA;
	for (; trazoH > 4; trazoH--) {
		pavimentarCasilla(trazoH, trazoV, direccion);
		direccion = -1;
	}
	direccion = ARRIBA;
	for (; trazoV >= 22; trazoV--) {
		pavimentarCasilla(trazoH, trazoV, direccion);
		direccion = -1;
	}
}

/**
 * Construye las calles que atraviesan los barrios Norte y Sur de Threadville.
 */
void construyeTransversalesG() {
	printf("\tConstruyendo cuadras y calles transversales...\n");
	GtkWidget *terreno;
	GList *children;
	int trazoH, trazoV;

	// Pintar fondo cuadras
	for (trazoH = 5; trazoH <= 50; trazoH++) {
		for (trazoV = 1; trazoV <= 6; trazoV++) {
			children = gtk_container_get_children(GTK_CONTAINER(gtk_grid_get_child_at(GTK_GRID(contenedorBase), trazoH, trazoV)));
			terreno = children->data;
			g_list_free(children);
			gtk_widget_override_background_color(terreno, GTK_STATE_NORMAL, &colorCuadra);

			children = gtk_container_get_children(GTK_CONTAINER(gtk_grid_get_child_at(GTK_GRID(contenedorBase), trazoH, trazoV + 7)));
			terreno = children->data;
			g_list_free(children);
			gtk_widget_override_background_color(terreno, GTK_STATE_NORMAL, &colorCuadra);

			children = gtk_container_get_children(GTK_CONTAINER(gtk_grid_get_child_at(GTK_GRID(contenedorBase), trazoH, trazoV + 21)));
			terreno = children->data;
			g_list_free(children);
			gtk_widget_override_background_color(terreno, GTK_STATE_NORMAL, &colorCuadra);

			children = gtk_container_get_children(GTK_CONTAINER(gtk_grid_get_child_at(GTK_GRID(contenedorBase), trazoH, trazoV + 28)));
			terreno = children->data;
			g_list_free(children);
			gtk_widget_override_background_color(terreno, GTK_STATE_NORMAL, &colorCuadra);
		}
	}

	// Trazar transversales
	int direccion = IZQUIERDA;
	for (trazoH = 50, trazoV = 7; trazoH >= 5; trazoH--) {
		pavimentarCasilla(trazoH, trazoV, direccion);
		direccion = -1;
	}
	direccion = DERECHA;
	for (trazoH = 5, trazoV = 28; trazoH <= 50; trazoH++) {
		pavimentarCasilla(trazoH, trazoV, direccion);
		direccion = -1;
	}
}

/**
 * Construye las avenidas de Threadville.
 */
void construyeAvenidasG() {
	printf("\tConstruyendo avenidas...\n");
	int trazoH = 11;
	int trazoV;
	int direccion;

	// Avenidas del Norte
	while (trazoH <= 44) {
		direccion = ABAJO;
		for (trazoV = 1; trazoV <= 13; trazoV++) {
			pavimentarCasilla(trazoH, trazoV, direccion);
			direccion = -1;
			if (trazoV == 6) {
				trazoV++;
				direccion = ABAJO;
			}
		}
		trazoH++;
		direccion = ARRIBA;
		for (trazoV = 13; trazoV >= 1; trazoV--) {
			pavimentarCasilla(trazoH, trazoV, direccion);
			direccion = -1;
			if (trazoV == 8) {
				trazoV--;
				direccion = ARRIBA;
			}
		}
		trazoH += 7;
	}

	trazoH = 44;

	//Avenidas del sur
	while (trazoH >= 11) {
		direccion = ARRIBA;
		for (trazoV = 34; trazoV >= 22; trazoV--) {
			pavimentarCasilla(trazoH, trazoV, direccion);
			direccion = -1;
			if (trazoV == 29) {
				trazoV--;
				direccion = ARRIBA;
			}
		}
		trazoH--;
		for (trazoV = 22; trazoV <= 34; trazoV++) {
			pavimentarCasilla(trazoH, trazoV, direccion);
			direccion = -1;
			if (trazoV == 27) {
				trazoV++;
				direccion = ABAJO;
			}
		}
		trazoH -= 7;
	}
}

/**
 * Construye los puentes.
 */
void construyePuentesG() {
	printf("\tConstruyendo puentes...\n");
	GtkWidget *overlayP;
	GList *children;
	int trazoH, trazoV;
	
	for(trazoH = 12; trazoH <= 44; trazoH += 8) {
		pavimentarCasilla(trazoH, 14, -1);
		for(trazoV = 15; trazoV <= 20; trazoV++) {
			pavimentarCasilla(trazoH, trazoV, -1);
			overlayP = gtk_grid_get_child_at(GTK_GRID(contenedorBase), trazoH, trazoV);
			children = gtk_container_get_children(GTK_CONTAINER(overlayP));
			gtk_widget_override_background_color(children->data, GTK_STATE_NORMAL, &colorPuente);
			g_list_free(children);
		}
		pavimentarCasilla(trazoH, 21, -1);
	}
}

gboolean pintarSemaforo_callback(gpointer datosObjeto) {
	DatosPuenteG *datosPuente = datosObjeto;
	int posH = (datosPuente->idPuente == P_CURLY) ? 19 : 35;
	int posVN = 14;
	int posVS = 21;
	GdkPixbuf *colorSemN = (datosPuente->estadoN) ? semaVerde : semaRojo;
	GdkPixbuf *colorSemS = (datosPuente->estadoS) ? semaVerde : semaRojo;
	GtkWidget *imaSemaforo;
	
	GList *children = gtk_container_get_children(
					GTK_CONTAINER(gtk_grid_get_child_at(GTK_GRID(contenedorBase), posH, posVN)));
	imaSemaforo = children->data;
	g_list_free(children);
	gtk_image_set_from_pixbuf(GTK_IMAGE(imaSemaforo), colorSemN);
	
	children = gtk_container_get_children(
					GTK_CONTAINER(gtk_grid_get_child_at(GTK_GRID(contenedorBase), posH, posVS)));
	imaSemaforo = children->data;
	g_list_free(children);
	gtk_image_set_from_pixbuf(GTK_IMAGE(imaSemaforo), colorSemS);
	
	datosPuente = NULL;
	g_free(datosObjeto);
	return G_SOURCE_REMOVE;
}
/**
 * Refresca el estado visual de los semáforos de los puentes Curly y Shemp.
 * @param puente
 * @param sNorte
 * @param sSur
 */
void pintarSemaforo(int puente, int sNorte, int sSur) {
	DatosPuenteG *datosP = g_malloc(sizeof(DatosPuenteG));
	datosP->idPuente = puente;
	datosP->estadoN = sNorte;
	datosP->estadoS = sSur;
	gdk_threads_add_idle(pintarSemaforo_callback, datosP);
}

gboolean pintarCuentaPuente_callback(gpointer datosObjeto) {
	DatosPuenteG *datosPuente = datosObjeto;
	int posH = (datosPuente->idPuente == P_LARRY) ? 11 : 43;
	int posVN = 14;
	int posVS = 21;
	GtkWidget *imaConteo;
	GtkWidget *labelConteo;
	char bufferCont[8];
	
	GtkWidget* overlayPuente = gtk_grid_get_child_at(GTK_GRID(contenedorBase), posH, posVN);
	GList *children = gtk_container_get_children(GTK_CONTAINER(overlayPuente));
	imaConteo = children->data;
	gtk_widget_override_background_color(imaConteo, GTK_STATE_NORMAL, &colorPolicia);
	snprintf(bufferCont, 8, "%d", datosPuente->estadoN);
	children = children->next;
	if (children) {
		labelConteo = children->data;
		gtk_label_set_text(GTK_LABEL(labelConteo), bufferCont);
	}
	else {
		labelConteo = gtk_label_new(bufferCont);
		gtk_overlay_add_overlay(GTK_OVERLAY(overlayPuente), labelConteo);
		gtk_widget_show(labelConteo);
	}
	g_list_free(children);
	
	overlayPuente = gtk_grid_get_child_at(GTK_GRID(contenedorBase), posH, posVS);
	children = gtk_container_get_children(GTK_CONTAINER(overlayPuente));
	imaConteo = children->data;
	gtk_widget_override_background_color(imaConteo, GTK_STATE_NORMAL, &colorPolicia);
	snprintf(bufferCont, 8, "%d", datosPuente->estadoS);
	children = children->next;
	if (children) {
		labelConteo = children->data;
		gtk_label_set_text(GTK_LABEL(labelConteo), bufferCont);
	}
	else {
		labelConteo = gtk_label_new(bufferCont);
		gtk_overlay_add_overlay(GTK_OVERLAY(overlayPuente), labelConteo);
		gtk_widget_show(labelConteo);
	}
	g_list_free(children);
	
	datosPuente = NULL;
	g_free(datosObjeto);
	return G_SOURCE_REMOVE;
}
/**
 * Refresca el estado visual de las cuentas de carros de los policías en los puentes Larry y Joe.
 * @param posH
 * @param posV
 * @param cuadra
 * @param numeroCuadra
 */
void pintarCuentaPuente(int puente, int cNorte, int cSur) {
	DatosPuenteG *datosP = g_malloc(sizeof(DatosPuenteG));
	datosP->idPuente = puente;
	datosP->estadoN = cNorte;
	datosP->estadoS = cSur;
	gdk_threads_add_idle(pintarCuentaPuente_callback, datosP);
}

/**
 * Pinta un puesto de parada.
 * @param posH
 * @param posV
 * @param cuadra
 * @param numeroCuadra
 */
void pintarParada(int posH, int posV, int cuadra, int numeroCuadra) {
	GtkWidget *labelParada;
	GtkWidget *overlayP = gtk_grid_get_child_at(GTK_GRID(contenedorBase), posH, posV);
	GList *children = gtk_container_get_children(GTK_CONTAINER(overlayP));
	GtkWidget *parada = children->data;
	g_list_free(children);
	
	gtk_widget_override_background_color(parada, GTK_STATE_NORMAL, &colorParada);
	
	if (cuadra < 89) {	// Menor a rotondas Y y Z
		snprintf(bufferEtiquetas, 8, "%c%d", (char)cuadra, numeroCuadra);
	}
	else {
		snprintf(bufferEtiquetas, 8, "%c", (char)cuadra);
	}
	labelParada = gtk_label_new(bufferEtiquetas);
	gtk_widget_set_halign(labelParada, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(labelParada, GTK_ALIGN_CENTER);
	//gtk_widget_override_font(labelParada, fuenteDefecto);
	//gtk_widget_override_color(labelParada, 0, &colorEtiqueta);
	gtk_overlay_add_overlay(GTK_OVERLAY(overlayP), labelParada);
}

/**
 * Marca las paradas en la interfaz gráfica.
 */
void construyeParadasG() {
	printf("\tConstruyendo paradas...\n");
	int cuadra;
	int cuadraH = 6;
	int cuadraV = 1;
	int numParada;
	
	for(cuadra = 65; cuadra <= 88; cuadra++) {
		numParada = 1;
		if (cuadraV != 22) {
			pintarParada(cuadraH, cuadraV, cuadra, numParada++);
			pintarParada(cuadraH + 3, cuadraV, cuadra, numParada++);
		}
		else {
			pintarParada(cuadraH - 1, cuadraV + 1, cuadra, numParada++);
		}
		pintarParada(cuadraH + 4, cuadraV + 1, cuadra, numParada++);
		pintarParada(cuadraH + 4, cuadraV + 4, cuadra, numParada++);
		if (cuadraV != 8) {
			pintarParada(cuadraH + 3, cuadraV + 5, cuadra, numParada++);
			pintarParada(cuadraH, cuadraV + 5, cuadra, numParada++);
		}
		pintarParada(cuadraH - 1, cuadraV + 4, cuadra, numParada++);
		if (cuadraV != 22) {
			pintarParada(cuadraH - 1, cuadraV + 1, cuadra, numParada);
		}
		
		if (cuadraH == 46) {
			cuadraH = 6;
			if (cuadraV == 8) {
				cuadraV += 14;
			}
			else {
				cuadraV += 7;
			}
		}
		else {
			cuadraH += 8;
		}
	}
	
	// Pintar paradas Y y Z
	pintarParada(1, 18, 89, 1);
	pintarParada(54, 17, 90, 1);
}

/**
 * Monta los contenedores y layouts básicos de la aplicación visual.
 * @param vBase
 */
void montarContenidoVentana(VentanaBase *vBase) {
	GtkWidget *overlayG;
	GtkWidget *imaG;

	contenedorBase = gtk_grid_new();
	gtk_grid_set_row_homogeneous(GTK_GRID(contenedorBase), 1);
	gtk_grid_set_column_homogeneous(GTK_GRID(contenedorBase), 1);

	int n, m;
	for (n = 0; n < ANCHO_DESPLIEGUE; n++) {
		for (m = 0; m < ALTO_DESPLIEGUE; m++) {
			overlayG = gtk_overlay_new();
			//gtk_widget_override_background_color(overlayG, GTK_STATE_NORMAL, &colorCalle);
			gtk_grid_attach(GTK_GRID(contenedorBase), overlayG, n, m, 1, 1);
			imaG = gtk_image_new();
			gtk_container_add(GTK_CONTAINER(overlayG), imaG);

			/*
			GtkWidget *labelCasilla;
			snprintf(bufferEtiquetas, 8, "%d/%d", n, m);
			labelCasilla = gtk_label_new(bufferEtiquetas);
			gtk_widget_override_font(labelCasilla, fuenteDefecto);
			gtk_widget_override_color(labelCasilla, 0, &colorEtiqueta);
			gtk_widget_set_halign(labelCasilla, GTK_ALIGN_START);
			gtk_widget_set_valign(labelCasilla, GTK_ALIGN_START);
			gtk_overlay_add_overlay(GTK_OVERLAY(overlayG), labelCasilla);
			*/
		}
	}

	construyeAutopistaG();
	construyeCircunvalacionG();
	construyeTransversalesG();
	construyeAvenidasG();
	construyePuentesG();
	
	construyeParadasG();

	montarContenedorIzquierda(vBase);
	montarContenedorDerecha(vBase);

	for (n = 0; n < 4; n++) {
		for (m = 0; m < 14; m++) {
			gtk_widget_destroy(GTK_WIDGET(gtk_grid_get_child_at(GTK_GRID(contenedorBase), n, m)));
		}
	}
	for (n = ANCHO_DESPLIEGUE - 4; n < ANCHO_DESPLIEGUE; n++) {
		for (m = 0; m < 14; m++) {
			gtk_widget_destroy(GTK_WIDGET(gtk_grid_get_child_at(GTK_GRID(contenedorBase), n, m)));
		}
	}
	gtk_grid_attach(GTK_GRID(contenedorBase), vBase->seccionIzquierda, 0, 0, 4, 14);
	gtk_grid_attach(GTK_GRID(contenedorBase), vBase->seccionDerecha, ANCHO_DESPLIEGUE - 4, 0, 4, 14);

	gtk_container_add(GTK_CONTAINER(vBase->window), contenedorBase);
}

void montarContenidoDialogo(VentanaBase *vBase) {
	char bufferOp[8];
	controlesDialogo.listaParadas = gtk_list_store_new(1, G_TYPE_STRING);
	int i, j, maxJ;
	for(i = 65; i <= 88; i++) {
		maxJ = (i < 71 || i > 82) ? 8 : 6;
		for(j = 1; j <= maxJ; j++) {
			snprintf(bufferOp, 8, "%c%d", (char)i, j);
			gtk_list_store_insert_with_values(controlesDialogo.listaParadas, NULL, -1,
				0, bufferOp,
			-1);
		}
	}
	gtk_list_store_insert_with_values(controlesDialogo.listaParadas, NULL, -1,
		0, "Y",
	-1);
	gtk_list_store_insert_with_values(controlesDialogo.listaParadas, NULL, -1,
		0, "Z",
	-1);
}

void inicializarRecursos() {
	gdk_rgba_parse(&colorPasto, "#9AB04C");
	gdk_rgba_parse(&colorTransp, "rgba(0,0,0,0)");
	gdk_rgba_parse(&colorCalle, "#828385");
	gdk_rgba_parse(&colorPuente, "#D8DEEB");
	
	gdk_rgba_parse(&colorEtiqueta, "#FFFF66");
	gdk_rgba_parse(&colorEtiquetaCarro, "#FFFFFF");
	gdk_rgba_parse(&colorEtiquetaBus, "#D41FDE");
	gdk_rgba_parse(&colorCuadra, "#BDA80F");
	gdk_rgba_parse(&colorParada, "#FFFFFF");
	
	gdk_rgba_parse(&fuenteRepa, "#F5F70B");
	//gdk_rgba_parse(&fondoRepa, "#FFFF00");
	gdk_rgba_parse(&fondoRepa, "rgba(0,0,0,0)");
	
	gdk_rgba_parse(&colorActivo, "#00FF00");
	gdk_rgba_parse(&colorDesactivo, "#FF0000");
	
	gdk_rgba_parse(&colorPolicia, "#07DEDA");
	
	fuenteDefecto = pango_font_description_new();
	pango_font_description_set_size(fuenteDefecto, 7 * PANGO_SCALE);
	
	fuenteVehiculo = pango_font_description_new();
	pango_font_description_set_size(fuenteVehiculo, 8 * PANGO_SCALE);
	pango_font_description_set_weight(fuenteVehiculo, PANGO_WEIGHT_BOLD);
	
	fuenteReparacion = pango_font_description_new();
	pango_font_description_set_size(fuenteReparacion, 10 * PANGO_SCALE);
	
	iconoP2 = create_pixbuf(ICONO_P2);
	
	semaVerde = create_pixbuf(SEMA_VERDE);
	semaVerde = gdk_pixbuf_scale_simple(semaVerde, 20, 20, GDK_INTERP_BILINEAR);
	semaRojo = create_pixbuf(SEMA_ROJO);
	semaRojo = gdk_pixbuf_scale_simple(semaRojo, 20, 20, GDK_INTERP_BILINEAR);
	
	int i;
	for(i = 0; i <= AMBULANCIA; i++) {
		imaCarro[i] = create_pixbuf(IMA_CARROS_G[i]);
		imaCarro[i] = gdk_pixbuf_scale_simple(imaCarro[i], 20, 20, GDK_INTERP_BILINEAR);
	}
	
	for(i = 0; i < 4; i++) {
		DIRECCIONES[i] = create_pixbuf(DIR_FILES[i]);
		DIRECCIONES[i] = gdk_pixbuf_scale_simple(DIRECCIONES[i], 20, 20, GDK_INTERP_BILINEAR);
	}
}

void switchBus(GtkWidget *widget, gpointer data) {
	int *idBus = data;
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(switchesBus[*idBus]))) {
		printf("Creando el bus %d\n", *idBus);
		crearBus(*idBus);
	}
	else {
		printf("Destruyendo el bus %d\n", *idBus);
		matarBus(*idBus);
	}
}

void switchReparaciones(GtkWidget *widget) {
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(switchRepa))) {
		empezarReparaciones();
		gtk_widget_override_color(switchRepa, GTK_STATE_NORMAL, &colorActivo);
	}
	else {
		pararReparaciones();
		gtk_widget_override_color(switchRepa, GTK_STATE_NORMAL, &colorDesactivo);
	}
}

/**
 * Inicia la ejecución de los hilos de autobuses.
 */
void windowReady(GtkWidget *widget, gpointer data) {
	int i;
	for(i = B_ROJO; i <= B_NARANJA; i++) {
		crearBus(i);
	}
	empezarReparaciones();
}

/**
 * Limpia los recursos gráficos.
 * @param widget
 * @param vBase
 */
void limpiarGTK(GtkWidget *widget, VentanaBase *vBase) {
	liberarRecursosEntidades(); // Hilos creados en entidades.c
	printf("Recursos gráficos liberados.\n");
	gtk_main_quit();
}

/**
 * Inicializa el GTK y construye la pantalla principal.
 * @param argc
 * @param argv
 */
void inicializarGUI(int *argc, char ***argv) {
	gtk_init(argc, argv);
	VentanaBase vBase;

	inicializarRecursos();
	
	/* Instancia de ventana principal */
	vBase.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_icon(GTK_WINDOW(vBase.window), iconoP2);
	g_signal_connect(vBase.window, "destroy", G_CALLBACK(limpiarGTK), &vBase);
	g_signal_connect (vBase.window, "show", G_CALLBACK(windowReady), NULL);

	/* Configuración ventana principal */
	gtk_window_set_title(GTK_WINDOW(vBase.window), "Proyecto 2 - ramirez-sanchez-serrato");

	/* Tamaño ventana */
	GdkScreen *screen = gdk_screen_get_default();
	gint height = gdk_screen_get_height(screen);
	gint width = gdk_screen_get_width(screen);
	gtk_window_set_default_size(GTK_WINDOW(vBase.window), width, height);
	gtk_widget_realize(vBase.window);
	gtk_window_fullscreen(GTK_WINDOW(vBase.window));

	gtk_widget_override_background_color(vBase.window, GTK_STATE_NORMAL, &colorPasto);

	printf("Graficando Threadville:\n");
	montarContenidoVentana(&vBase);
	
	montarContenidoDialogo(&vBase);

	/* Mostrar todos los componentes y correr el loop de GTK */
	gtk_widget_show_all(vBase.window);
	gtk_main();
}
