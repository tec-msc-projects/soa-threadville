/******************************************************************************
* ARCHIVO: gui.h
* DESCRIPCIÓN:
* 	Archivo con la declaración de los métodos públicos de manejo gráfico de la
*		aplicación.
*   
* AUTORES:
* 	Mauricio Ramírez
* 	Paula Sánchez
* 	Johan Serrato
******************************************************************************/
#ifndef GUI_H
	#define GUI_H

/* FUNCIONES GUI */
void inicializarGUI(int *, char ***);

void avanzarBusG(ColorBus, int, int, int, const char *);

void matarBusG(int, int);

void avanzarAutomovilG(ColorAuto, int, int, const char *, int);

void matarAutomovilG(int);

void reparacionG(int, int);

void quitarReparacionG(int);

void pintarSemaforo(int, int, int);

void pintarCuentaPuente(int, int, int);

#endif /* GUI_H */