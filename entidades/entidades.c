/******************************************************************************
 * ARCHIVO: entidades.c
 * DESCRIPCIÓN:
 * 	Archivo con los métodos de manejo de hilos que representan entidades en Threadville.
 *   
 * AUTORES:
 * 	Mauricio Ramírez
 * 	Paula Sánchez
 * 	Johan Serrato
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>	//randNumber
#include <math.h>
#include <string.h>

#include "../defGlobal.h"
#include "entidades.h"
#include "../gui/gui.h"

/* CONSTANTES */
double TIEMPO_CARRO_ROJO = 25000; // 25ms
double VELOCIDAD_BUS[9] = { 5, 5, 5, 4, 4, 4, 3, 3, 7 };
double VELOCIDAD_AUTO[6] = { 1, 2, 3, 4, 5, 6 };
double VELOCIDAD_AMBULANCIA = 0.5;

int RUTA_BUS_ROJO[] = {
	239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,
	263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,
	287,288,289,290,291,292,293,294,295,296,297,
	118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,112,113,114,115,116,117,
	118,119,120,121,122,123,124,125,126,127,
	298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,
	322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,
	346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,
	370,371,
	7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
	224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239
};
int RUTA_BUS_VERDE[] = {
	274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,
	118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,112,113,114,115,116,117,
	118,119,120,
	136,138,140,142,144,146,148,150,152,154,156,158,160,162,164,166,168,170,172,174,176,178,180,182,
	184,186,188,190,192,194,196,198,200,202,204,206,208,210,212,214,216,218,220,222,
	14,15,16,17,18,19,20,21,22,23,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
	224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,
	248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,
	272,273,274
};
int RUTA_BUS_AZUL[] = {
	302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,
	326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,
	350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,
	7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,0,1,2,3,4,5,6,7,8,9,
	24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80,82,84,86,
	88,90,92,94,96,98,100,102,104,106,108,110,
	125,126,127,128,129,130,131,132,133,134,135,112,113,114,115,116,117,118,119,120,121,122,123,124,
	125,126,127,
	298,299,300,301,302
};
int RUTA_BUS_BLANCO[] = {
	522,523,
	720,721,722,723,724,725,726,727,
	644,645,646,647,648,649,440,650,651,652,653,654,655,
	335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,
	680,681,682,683,684,685,425,686,687,688,689,690,691,
	711,710,709,708,707,706,705,704,
	476,477,478,479,480,481,410,482,483,484,485,486,487,
	245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,
	512,513,514,515,516,517,395,518,519,520,521,522
};
int RUTA_BUS_GRIS[] = {
	337,338,339,340,341,342,
	656,657,658,659,660,661,433,662,663,664,665,666,667,
	719,718,717,716,715,714,713,712,
	500,501,502,503,504,505,402,506,507,508,509,510,511,
	253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,
	536,537,538,539,540,541,387,542,543,544,545,546,547,
	728,729,730,731,732,733,734,735,
	620,621,622,623,624,625,448,626,627,628,629,630,631,
	327,328,329,330,331,332,333,334,335,336,337
};
int RUTA_BUS_NEGRO[] = {
	528,529,394,530,531,532,533,534,535,
	261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,
	560,561,562,563,564,565,379,566,567,568,569,570,571,
	736,737,738,739,740,741,742,743,
	596,597,598,599,600,601,456,602,603,604,605,606,607,
	319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,
	632,633,634,635,636,637,441,638,639,640,641,642,643,
	727,726,725,724,723,722,721,720,
	524,525,526,527,528
};
int RUTA_BUS_ROSA[] = {
	373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,
	397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,
	230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,
	254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,
	278,279,280,281,282,283,284,285,286,287,288,289,290,291,
	372,373
};
int RUTA_BUS_CELESTE[] = {
	356,357,358,359,360,361,362,363,364,365,
	418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,
	442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,
	304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,
	328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,
	352,353,354,355,356
};
int RUTA_BUS_NARANJA[] = {
	696,697,
	424,425,426,427,428,429,430,431,432,433,
	662,663,664,665,666,667,
	719,718,717,716,715,714,713,712,
	
	500,501,502,503,504,505,402,506,507,508,509,510,511,
	253,254,255,256,257,258,259,260,
	512,513,514,515,516,517,395,518,519,520,521,522,523,
	720,721,722,723,724,725,726,727,
	
	644,645,646,647,648,649,
	440,441,442,443,444,445,446,447,448,449,
	614,615,616,617,618,619,
	735,734,733,732,731,730,729,728,
	
	548,549,550,551,552,553,386,554,555,556,557,558,559,
	269,270,271,272,273,274,275,276,
	560,561,562,563,564,565,379,566,567,568,569,570,571,
	736,737,738,739,740,741,742,743,
	
	596,597,598,599,600,601,
	456,457,458,459,460,461,462,463,
	304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,
	584,585,586,587,588,589,457,590,591,592,593,594,595,
	743,742,741,740,739,738,737,736,
	
	572,573,574,575,576,577,
	378,379,380,381,382,383,384,385,386,387,
	542,543,544,545,546,547,
	728,729,730,731,732,733,734,735,
	
	620,621,622,623,624,625,448,626,627,628,629,630,631,
	327,328,329,330,331,332,333,334,
	632,633,634,635,636,637,441,638,639,640,641,642,643,
	727,726,725,724,723,722,721,720,
	
	524,525,526,527,528,529,
	394,395,396,397,398,399,400,401,402,403,
	494,495,496,497,498,499,
	712,713,714,715,716,717,718,719,
	
	668,669,670,671,672,673,432,674,675,676,677,678,679,
	343,344,345,346,347,348,349,350,
	680,681,682,683,684,685,425,686,687,688,689,690,691,
	711,710,709,708,707,706,705,704,
	
	476,477,478,479,480,481,
	410,411,412,413,414,415,416,417,
	230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,
	464,465,466,467,468,469,411,470,471,472,473,474,475,
	704,705,706,707,708,709,710,711,
	
	692,693,694,695,696
};

int PARADAS_BUS_ROJO[] = { 239, 263, 282, 296, 135, 299, 313, 337, 356, 370, 0, 225, 239 };
int PARADAS_BUS_VERDE[] = { 274, 293, 135, 0, 228, 247, 274 };
int PARADAS_BUS_AZUL[] = { 302, 321, 348, 367, 0, 135 ,302 };
int PARADAS_BUS_BLANCO[] = { 522, 648, 345, 690, 480, 255, 522 };
int PARADAS_BUS_GRIS[] = { 337, 666, 504, 263, 546, 624, 337 };
int PARADAS_BUS_NEGRO[] = { 528, 271, 570, 600, 329, 642, 528 };
int PARADAS_BUS_ROSA[] = { 373, 416, 239, 282, 373 };
int PARADAS_BUS_CELESTE[] = { 356, 419, 462, 313, 356 };
int PARADAS_BUS_NARANJA[] = {
	696, 663, 510, 513, 648, 615, 558, 561, 600, 309, 585,
	576, 543, 630, 633, 528, 495, 678, 681, 480, 235, 465, 696
};
int RUTA_Y_Z[] = {
	10,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83,85,
	87,89,91,93,95,97,99,101,103,105,107,109,111,124,125,126,127
};
int RUTA_Z_Y[] = {
	121,137,139,141,143,145,147,149,151,153,155,157,159,161,163,165,167,169,171,173,175,177,179,181,
	183,185,187,189,191,193,195,197,199,201,203,205,207,209,211,213,215,217,219,221,223,13,14,15,16
};

/* ESTRUCTURAS*/
typedef struct {
	int placa;
	pthread_t threadBus;
	int *ruta; //Ruta paso a paso a seguir por el bus
	int casillaActual; // Puntero al id de la casilla donde el bus se encuentra
	int casillaAnterior; // La casilla donde está actualmente la cola del bus
	int *destinos; // Arreglo con las paradas a visitar
	int siguienteDestino; //Puntero del arreglo *destinos
	double velocidad;
	ColorBus color;
} Autobus;

typedef struct {
	int placa;
	pthread_t threadVehiculo;
	int casillaActual; // Indica el id de la casilla donde el auto se encuentra
	char **destinos; // Arreglo con las paradas a visitar
	int siguienteDestino; //Puntero del arreglo *destinos
	int viajesRestantes; //Número que indica cuántos viajes quedan por realizar
	int *caminoOptimo;	//El camino óptimo a seguir por el auto en un viaje
	int siguienteCasilla; //Puntero del camino óptimo
	int esAmbulancia; // 1 si es ambulancia
	unsigned int velocidad;
	ColorAuto color;
} Vehiculo;

typedef struct {
	NombrePuente puenteID;
	pthread_t admin;
	int cuentaVehiculos;
	int direccion;
	int entradaN;
	int entradaS;
} Puente;

typedef struct {
	pthread_t threadReparacion;
	int tiempoReparacion;
	int casilla;
} Reparacion;

/* Variables globales */
int registroPlacas = 10;
Autobus buses[9];
Puente puentes[5];
int kLarryJoe = 3;
int mCurly = 15;
int nShemp = 10;
int mediaReparaciones = 40;
int maxDestinos = 15;
int cantidadFija = 0;
double sesgoRotondas = 1.24; //Recomendable entre 1 y 2 inclusive. Nunca menor a 1.

pthread_t generadorReparaciones;

pthread_mutex_t mutexThreaville;
pthread_mutex_t mutexParametros;
pthread_mutex_t mutexRegistroVehicular;

/**
 * Handler para cancelar el thread de un bus.
 * @param arg
 */
static void desaparcerBus(void *arg) {
	Autobus *bus = arg;
	//pthread_mutex_lock(&mutexThreaville);
	threadville[bus->ruta[bus->casillaActual]].estaOcupada = 0;
	threadville[bus->casillaAnterior].estaOcupada = 0;
	if(threadville[bus->ruta[bus->casillaActual]].tipoEspacio == PUENTE) {
		puentes[threadville[bus->ruta[bus->casillaActual]].nomPuente].cuentaVehiculos--;
	}
	
	if(threadville[bus->ruta[bus->casillaActual + 1]].tipoEspacio == INT_NORMAL) {
		Casilla *casillaInt = &(threadville[bus->ruta[bus->casillaActual + 1]]);
		int siguienteInt = bus->casillaActual + 2;
		int restante;
		int espActual = casillaInt->pFirstOut;
		int espSiguiente = ((espActual + 1) > 3) ? 0 : espActual + 1;
		int llegoAPlaca = 0;
		do {
			for(restante = 4; restante > 0; restante--) {
				if (casillaInt->colaInt[espActual] == bus->placa) {
					llegoAPlaca = 1;
				}
				if(llegoAPlaca) {
					if (espSiguiente == casillaInt->pFirstOut) {
						casillaInt->colaInt[espActual] = 0;
						if (--(casillaInt->pCola) < 0) { casillaInt->pCola = 3; }
						printf("Llegó a la cola: restante=%d\n", restante);
					}
					else {
						casillaInt->colaInt[espActual] = casillaInt->colaInt[espSiguiente];
					}
				}
				if(++espSiguiente > 3) { espSiguiente = 0; }
				if(++espActual > 3) { espActual = 0; }
			}
			printf("Entró para la casilla %d\n", casillaInt->idCasilla);
			casillaInt = &(threadville[bus->ruta[siguienteInt++]]);
		}while (casillaInt->tipoEspacio == INT_NORMAL);
	}
	//pthread_mutex_unlock(&mutexThreaville);
	
	matarBusG(bus->ruta[bus->casillaActual], bus->casillaAnterior);
	printf("Desaparce el bus %d\n", bus->color);
}

/**
 * Thread de bus.
 * @param propiedades
 */
void *threadBus(Autobus *bus) {
	printf("Aparece el bus %d\n\n", bus->color);
	pthread_cleanup_push(desaparcerBus, bus);
	
	unsigned int velocidadBus = TIEMPO_CARRO_ROJO * VELOCIDAD_BUS[bus->color];
	int cambioDestino = 0;
	int casillaSiguiente = bus->ruta[bus->casillaActual];
	int casillaActual = bus->ruta[bus->casillaActual] - 1;
	bus->casillaAnterior = casillaActual;
	Casilla *extremoPuente;
	int reservoCruceP = 0;
	
	Casilla *interSiguiente;
	int siguienteInt, tengoPrioridadInt, iRevision;
	
	//int temp = 0;
	do {
		if(threadville[casillaSiguiente].tipoEspacio == INT_PUENTE) {
			extremoPuente = &(threadville[casillaSiguiente]);
			//Si voy entrando...
			if(threadville[bus->ruta[bus->casillaActual + 2]].tipoEspacio == PUENTE) {
				while(1) {
					pthread_mutex_lock(&mutexThreaville);
					extremoPuente->arribo = 1;
					if ((!extremoPuente->estaOcupada) && (extremoPuente->semaforoVerde)
									&& (extremoPuente->cuentaOfiTran != 0)) {
						if (puentes[extremoPuente->nomPuente].cuentaVehiculos == 0
										|| (puentes[extremoPuente->nomPuente].direccion != extremoPuente->dirEntrada)) {
							puentes[extremoPuente->nomPuente].cuentaVehiculos++;
							puentes[extremoPuente->nomPuente].direccion = !(extremoPuente->dirEntrada);
							if (extremoPuente->cuentaOfiTran > 0) {
								if (--extremoPuente->cuentaOfiTran > 0) {
									extremoPuente->cuentaOfiTran--;
								}
							}
							pthread_mutex_unlock(&mutexThreaville);
							reservoCruceP = 1;
							break;
						}
					}
					pthread_mutex_unlock(&mutexThreaville);
					usleep(velocidadBus);
				}
			}
			//Si voy saliendo...
			else if (threadville[casillaActual].tipoEspacio == PUENTE) {
				while(1) {
					pthread_mutex_lock(&mutexThreaville);
					if (!extremoPuente->estaOcupada) {
						puentes[extremoPuente->nomPuente].cuentaVehiculos--;
						pthread_mutex_unlock(&mutexThreaville);
						reservoCruceP = 1;
						break;
					}
					pthread_mutex_unlock(&mutexThreaville);
					usleep(velocidadBus);
				}
			}
		}
		
		// Si la siguiente casilla es una intersección
		if(threadville[casillaSiguiente].tipoEspacio == INT_NORMAL) {
			while(1) {
				siguienteInt = bus->casillaActual + 2;
				tengoPrioridadInt = 1;
				interSiguiente = &(threadville[casillaSiguiente]);
				pthread_mutex_lock(&mutexThreaville);
				do {
					if (interSiguiente->estaOcupada
									|| (interSiguiente->colaInt[interSiguiente->pFirstOut] != bus->placa)) {
						tengoPrioridadInt = 0;
						break;
					}
					interSiguiente = &(threadville[bus->ruta[siguienteInt]]);
					siguienteInt++;
				} while (interSiguiente->tipoEspacio == INT_NORMAL);
				//Cuando va a abandonar una casilla de intersección
				if(tengoPrioridadInt && !interSiguiente->estaOcupada && !threadville[bus->ruta[siguienteInt]].estaOcupada) {
					interSiguiente = &(threadville[casillaSiguiente]);
					interSiguiente->estaOcupada = 1;
					reservoCruceP = 1;
					interSiguiente->colaInt[interSiguiente->pFirstOut] = 0;
					if (++(interSiguiente->pFirstOut) > 3) { interSiguiente->pFirstOut = 0; }
					pthread_mutex_unlock(&mutexThreaville);
					break;
				}
				pthread_mutex_unlock(&mutexThreaville);
				usleep(velocidadBus);
			}
		}
		
		pthread_mutex_lock(&mutexThreaville);
		if (!(threadville[casillaSiguiente].estaOcupada) || reservoCruceP) {
			reservoCruceP = 0;
			threadville[casillaSiguiente].estaOcupada = 1;
			threadville[casillaActual].estaOcupada = 1;
			if (bus->casillaAnterior != casillaActual) { // Al aparecer un bus, no debe desocupar la cola
				threadville[bus->casillaAnterior].estaOcupada = 0;
			}
			
			// Para reservar recurso intersección si va a meterse a una
			if(!(casillaSiguiente == bus->destinos[bus->siguienteDestino])) {
				interSiguiente = &(threadville[bus->ruta[bus->casillaActual + 2]]);
				if(interSiguiente->tipoEspacio == INT_NORMAL) {
					siguienteInt = bus->casillaActual + 2;
					do {
						for(iRevision = 0; iRevision < 4; iRevision++) {
							if (interSiguiente->colaInt[iRevision] == bus->placa) { break; }
							if ((iRevision + 1) == 4) {
								interSiguiente->colaInt[interSiguiente->pCola] = bus->placa;
								if (++(interSiguiente->pCola) > 3) { interSiguiente->pCola = 0; }
							}
						}
						interSiguiente = &(threadville[bus->ruta[siguienteInt]]);
						siguienteInt++;
					} while(interSiguiente->tipoEspacio == INT_NORMAL);
				}
			}
			pthread_mutex_unlock(&mutexThreaville);
			
			if (casillaSiguiente == bus->destinos[bus->siguienteDestino]) {
				bus->siguienteDestino = (casillaSiguiente == bus->ruta[0]) ? 1 : bus->siguienteDestino + 1;
				cambioDestino = 1;
			}
			
			char *etiquetaDestino =
				(threadville[bus->destinos[bus->siguienteDestino]].etiquetaPD[0] != 0) ?
					threadville[bus->destinos[bus->siguienteDestino]].etiquetaPD :
					threadville[bus->destinos[bus->siguienteDestino]].etiquetaPI;
					
			avanzarBusG(bus->color, casillaActual, casillaSiguiente, bus->casillaAnterior, etiquetaDestino);
			bus->casillaActual++;
			if (casillaSiguiente == bus->ruta[0]) {
				bus->casillaActual = 0;
			}
			bus->casillaAnterior = casillaActual;

			casillaActual = bus->ruta[bus->casillaActual];
			casillaSiguiente = bus->ruta[bus->casillaActual + 1];
			
			//if (temp) { printf("BUS_%d esperó %d veces\n", bus->color, temp); }
			//temp = 0;
		}
		else {
			pthread_mutex_unlock(&mutexThreaville);
			//temp++;
		}
		pthread_testcancel();
		
		if (cambioDestino) {
			cambioDestino = 0;
			sleep(5);
		}
		else {
			if (threadville[casillaActual].tipoEspacio == AUTOPISTA) {
				usleep(velocidadBus / 2);
			}
			else {
				usleep(velocidadBus);
			}
		}
	} while(1);
	
	pthread_cleanup_pop(1);
	pthread_exit(NULL);
}
/**
 * Crea un thread de bus.
 */
void crearBus(int indiceBus) {
	Autobus *unBus = &buses[indiceBus];
	unBus->placa = indiceBus + 1;
	unBus->casillaActual = 0;
	unBus->siguienteDestino = 0;
	pthread_create(&(unBus->threadBus), NULL, (void *) threadBus, (Autobus *) unBus);
}
/**
 * Mata el thread del bus y lo saca de Threadville.
 */
void matarBus(int indiceBus) {
	int rc = pthread_cancel(buses[indiceBus].threadBus);
	if (!rc) {
		printf("El bus %d ha sido cancelado\n", indiceBus);
	}
}
/**
 * Genera la información de todos los buses.
 */
void generarBuses() {
	int *rutas[] = {
		RUTA_BUS_ROJO, RUTA_BUS_VERDE, RUTA_BUS_AZUL, RUTA_BUS_BLANCO, RUTA_BUS_GRIS,
		RUTA_BUS_NEGRO, RUTA_BUS_ROSA, RUTA_BUS_CELESTE, RUTA_BUS_NARANJA
	};
	int *paradas[] = {
		PARADAS_BUS_ROJO, PARADAS_BUS_VERDE, PARADAS_BUS_AZUL, PARADAS_BUS_BLANCO, PARADAS_BUS_GRIS,
		PARADAS_BUS_NEGRO, PARADAS_BUS_ROSA, PARADAS_BUS_CELESTE, PARADAS_BUS_NARANJA
	};
	Autobus *busActual;
	int b;
	for (b = 0; b <= B_NARANJA; b++) {
		busActual = &buses[b];
		busActual->color = b;
		busActual->ruta = rutas[b];
		busActual->destinos = paradas[b];
		busActual->velocidad = VELOCIDAD_BUS[b];
	}
}

/**
 * Thread para un vehículo.
 * @param vehiculo
 * @return 
 */
void *threadVehiculo(Vehiculo *vehiculo) {
	pthread_mutex_lock(&mutexRegistroVehicular);
	vehiculo->placa = registroPlacas++;
	pthread_mutex_unlock(&mutexRegistroVehicular);
	char *paradaOrigen;
	char *paradaDestino;
	int pOF, pOC, pDF, pDC, etiquetaViajes, i, cambioDestino, casillaAnterior;
	int allocDest = vehiculo->viajesRestantes;
	int reservoCruceP = 0;
	int carrilInterno = 0;
	int *bufferRuta = NULL;
	int bufferSiguienteCasilla = 0;
	Casilla *extremoPuente = NULL;
	Casilla *interSiguiente = NULL;
	int siguienteInt, tengoPrioridadInt, iRevision;
	printf("Aparece el vehículo %d!!!\n\n", vehiculo->placa);
	
	paradaOrigen = vehiculo->destinos[vehiculo->siguienteDestino];
	etiquetaViajes = vehiculo->viajesRestantes;
	cambioDestino = 0;
	casillaAnterior = 0;
	int tiempoParada = (vehiculo->esAmbulancia) ? 8 : 3;
	do {
		pOF = (int)paradaOrigen[0] - 65;
		pOC = (pOF < 24) ? (int)paradaOrigen[1] - 49 : 0;
		paradaDestino = vehiculo->destinos[vehiculo->siguienteDestino++];
		pDF = (int)paradaDestino[0] - 65;
		pDC = (pDF < 24) ? (int)paradaDestino[1] - 49 : 0;
		vehiculo->caminoOptimo = obtenerRutaOptima(paradas[pOF][pOC], paradas[pDF][pDC]);
		paradaOrigen = paradaDestino;
		
		vehiculo->siguienteCasilla = 0;
		
		do {
			casillaAnterior = vehiculo->casillaActual;
			vehiculo->casillaActual = vehiculo->caminoOptimo[vehiculo->siguienteCasilla++];
			if (vehiculo->casillaActual == paradas[pDF][pDC]) {
				if (vehiculo->viajesRestantes > 1) {
					paradaDestino = vehiculo->destinos[vehiculo->siguienteDestino];
				}
				cambioDestino = (vehiculo->viajesRestantes == allocDest
								|| vehiculo->viajesRestantes == 1) ? 0 : 1;
				etiquetaViajes = vehiculo->viajesRestantes - 1;
			}
			
			// Si la siguiente casilla es una intersección con un puente
			if(threadville[vehiculo->casillaActual].tipoEspacio == INT_PUENTE) {
				extremoPuente = &(threadville[vehiculo->casillaActual]);
				
				//Si voy entrando...
				if(threadville[vehiculo->caminoOptimo[vehiculo->siguienteCasilla]].tipoEspacio == PUENTE) {
					while(1) {
						pthread_mutex_lock(&mutexThreaville);
						extremoPuente->arribo = 1;
						if ( vehiculo->esAmbulancia || ((!extremoPuente->estaOcupada) && (extremoPuente->semaforoVerde)
										&& (extremoPuente->cuentaOfiTran != 0))) {
							if (puentes[extremoPuente->nomPuente].cuentaVehiculos == 0
											|| (puentes[extremoPuente->nomPuente].direccion != extremoPuente->dirEntrada)) {
								puentes[extremoPuente->nomPuente].cuentaVehiculos++;
								puentes[extremoPuente->nomPuente].direccion = !(extremoPuente->dirEntrada);
								if (extremoPuente->cuentaOfiTran > 0) {
									extremoPuente->cuentaOfiTran--;
								}
/*
								printf("[Entrando Puente %d] Cuenta vehículos = %d | dirección = %d\n",
												extremoPuente->nomPuente,
												puentes[extremoPuente->nomPuente].cuentaVehiculos,
												puentes[extremoPuente->nomPuente].direccion);
*/
								pthread_mutex_unlock(&mutexThreaville);
								reservoCruceP = 1;
								break;
							}
						}
						pthread_mutex_unlock(&mutexThreaville);
						usleep(vehiculo->velocidad);
					}
				}
				//Si voy saliendo...
				else if (threadville[casillaAnterior].tipoEspacio == PUENTE) {
					while(1) {
						pthread_mutex_lock(&mutexThreaville);
						if (!extremoPuente->estaOcupada) {
							puentes[extremoPuente->nomPuente].cuentaVehiculos--;
		/*
							printf("[Saliendo Puente %d] Cuenta vehículos = %d | dirección = %d\n",
								extremoPuente->nomPuente,
								puentes[extremoPuente->nomPuente].cuentaVehiculos,
								puentes[extremoPuente->nomPuente].direccion);
		*/
							pthread_mutex_unlock(&mutexThreaville);
							reservoCruceP = 1;
							break;
						}
						pthread_mutex_unlock(&mutexThreaville);
						usleep(vehiculo->velocidad);
					}
				}
			}
			
			// Si la siguiente casilla es una intersección
			if(threadville[vehiculo->casillaActual].tipoEspacio == INT_NORMAL) {
				while(1) {
					siguienteInt = vehiculo->siguienteCasilla;
					tengoPrioridadInt = 1;
					interSiguiente = &(threadville[vehiculo->casillaActual]);
					pthread_mutex_lock(&mutexThreaville);
					do {
						if (interSiguiente->estaOcupada
										|| (!vehiculo->esAmbulancia /* Por prioridad de ambulancia */
											&& interSiguiente->colaInt[interSiguiente->pFirstOut] != vehiculo->placa)) {
							tengoPrioridadInt = 0;
							break;
						}
						interSiguiente = &(threadville[vehiculo->caminoOptimo[siguienteInt++]]);
					}while (interSiguiente->tipoEspacio == INT_NORMAL);
					if(tengoPrioridadInt && !interSiguiente->estaOcupada) {
						interSiguiente = &(threadville[vehiculo->casillaActual]);
						interSiguiente->estaOcupada = 1;
						reservoCruceP = 1;
						if (!vehiculo->esAmbulancia) {
							interSiguiente->colaInt[interSiguiente->pFirstOut] = 0;
							if (++(interSiguiente->pFirstOut) > 3) { interSiguiente->pFirstOut = 0; }
						}
						pthread_mutex_unlock(&mutexThreaville);
						break;
					}
					pthread_mutex_unlock(&mutexThreaville);
					usleep(vehiculo->velocidad);
				}
			}
			
			while(1) {
				pthread_mutex_lock(&mutexThreaville);
				if (!(threadville[vehiculo->casillaActual].estaOcupada) || reservoCruceP
								|| (casillaAnterior == vehiculo->casillaActual 
								&& (vehiculo->viajesRestantes != allocDest))) {
					threadville[casillaAnterior].estaOcupada = 0;
					threadville[vehiculo->casillaActual].estaOcupada = 1;
					reservoCruceP = 0;
					
					// Para reservar recurso intersección si va a meterse a una
					if(!vehiculo->esAmbulancia && vehiculo->casillaActual != paradas[pDF][pDC]) {
						interSiguiente = &(threadville[vehiculo->caminoOptimo[vehiculo->siguienteCasilla]]);
						if(interSiguiente->tipoEspacio == INT_NORMAL) {
							siguienteInt = vehiculo->siguienteCasilla;
							do {
								for(iRevision = 0; iRevision < 4; iRevision++) {
									if (interSiguiente->colaInt[iRevision] == vehiculo->placa) { break; }
									if ((iRevision + 1) == 4) {
										interSiguiente->colaInt[interSiguiente->pCola] = vehiculo->placa;
										if (++(interSiguiente->pCola) > 3) { interSiguiente->pCola = 0; }
									}
								}
								interSiguiente = &(threadville[vehiculo->caminoOptimo[siguienteInt++]]);
							} while(interSiguiente->tipoEspacio == INT_NORMAL);
						}
					}
					pthread_mutex_unlock(&mutexThreaville);
					break;
				}
				else {
					pthread_mutex_unlock(&mutexThreaville);
					usleep(vehiculo->velocidad);
				}
			}

			avanzarAutomovilG(vehiculo->color, casillaAnterior, vehiculo->casillaActual, paradaDestino,
							etiquetaViajes);

			// El auto escoge por cuál carril de la autopista ir, si por el que da Floyd (externo) o el interno
			if(threadville[vehiculo->casillaActual].tipoEspacio == INT_ENT_PISTA &&
					threadville[vehiculo->caminoOptimo[vehiculo->siguienteCasilla]].tipoEspacio == AUTOPISTA) {
				if(rand() % 2) {
					bufferRuta = vehiculo->caminoOptimo;
					bufferSiguienteCasilla = vehiculo->siguienteCasilla + 47;
					vehiculo->caminoOptimo = (threadville[vehiculo->casillaActual].idCasilla == 9) ? RUTA_Y_Z : RUTA_Z_Y;
					vehiculo->siguienteCasilla = 0;
					carrilInterno = 1;
				}
			}
			if(carrilInterno && vehiculo->siguienteCasilla == 49) {
				vehiculo->caminoOptimo = bufferRuta;
				bufferRuta = NULL;
				vehiculo->siguienteCasilla = bufferSiguienteCasilla;
				carrilInterno = 0;
			}
			
			if (cambioDestino) {
				cambioDestino = 0;
				sleep(tiempoParada);
			} else if (threadville[vehiculo->casillaActual].tipoEspacio == AUTOPISTA) {
				usleep(vehiculo->velocidad / 2);
			}
			else {
				usleep(vehiculo->velocidad);
			}
		} while(vehiculo->casillaActual != paradas[pDF][pDC]);
		
		free(vehiculo->caminoOptimo);
		if (--vehiculo->viajesRestantes == 0) {
			pthread_mutex_lock(&mutexThreaville);
			threadville[vehiculo->casillaActual].estaOcupada = 0;
			pthread_mutex_unlock(&mutexThreaville);
			
			matarAutomovilG(vehiculo->casillaActual);
		}
	} while(vehiculo->viajesRestantes > 0);

	for (i = 0; i < allocDest; i++) {
		//printf("Posición %d: %s\n", i, vehiculo->destinos[i]);
		free(vehiculo->destinos[i]);
	}
	free(vehiculo->destinos);
	
	printf("Muere el vehículo %d\n\n", vehiculo->placa);
	free(vehiculo);
	pthread_exit(NULL);
}

/**
 * Crea un thread de auto con los parámetros especificados.
 */
void crearVehiculo(ColorAuto color, int esAmbulancia, char **rutaManual, int tamArregloM) {
	Vehiculo *unVehiculo = (Vehiculo*) malloc(sizeof(Vehiculo));
	unVehiculo->destinos = rutaManual;
	rutaManual = NULL;
	
	unVehiculo->siguienteDestino = 0;
	unVehiculo->casillaActual = 0;
	unVehiculo->color = color;
	unVehiculo->esAmbulancia = esAmbulancia;
	unVehiculo->velocidad = (unVehiculo->esAmbulancia) ? TIEMPO_CARRO_ROJO * VELOCIDAD_AMBULANCIA :
		TIEMPO_CARRO_ROJO * VELOCIDAD_AUTO[unVehiculo->color];
	unVehiculo->viajesRestantes = tamArregloM;
	
	pthread_create(&(unVehiculo->threadVehiculo), NULL, (void *) threadVehiculo, (Vehiculo *) unVehiculo);
}
/**
 * Genera un auto con propiedades aleatorias.
 */
void generarVehiculo(int esAmbulancia) {
	ColorAuto color = (esAmbulancia) ? AMBULANCIA : (rand() % (A_AMARILLO + 1));
	int cantidadDestinos = 2;
	if (esAmbulancia) { cantidadDestinos += 2; }
	else if (cantidadFija) { cantidadDestinos += maxDestinos; }
	else if (maxDestinos > 0) { cantidadDestinos += ((rand() % maxDestinos) + 1); }
	
	int randCuadra, randNumPar, maxPar, sesgo;
	char bufPar[8];
	
	char **listaParadas = malloc(cantidadDestinos * sizeof(char*));
	int i;
	for (i = 0; i < cantidadDestinos; i++) {
    listaParadas[i] = malloc((8 + 1) * sizeof(char));
	}
	i = 0;
	snprintf(listaParadas[i++], 9, "%s", "Y");
	//printf("Destinos de un carro:\n");
	
	// Factor para el rango de escogencia de rotondas. Para las ambulancias, como sólo son 2 destinos,
	// mejor no aplicar el sesgo para evitar sobrecargar las rotondas por el tiempo que tardan.
	int sesgoAplicado = (esAmbulancia) ? 1 : sesgoRotondas;
	while(i < cantidadDestinos - 1) {
		randCuadra = rand() % (int)(26 * sesgoAplicado);
		if (randCuadra >= 24) {
			sesgo = randCuadra % 2;
			snprintf(bufPar, 8, "%c", (char)(sesgo + 24 + 65) );
		}
		else {
			if (randCuadra < 6 || randCuadra > 17) {
				maxPar = 8;
			}
			else {
				maxPar = 6;
			}
			randNumPar = (rand() % maxPar) + 1;
			snprintf(bufPar, 8, "%c%d", (char)(randCuadra + 65), randNumPar);
		}
		//printf("->%s", bufPar);
		snprintf(listaParadas[i++], 9, "%s", bufPar);
	}
	snprintf(listaParadas[i], 9, "%s", "Z");
	//printf("\n");
	
	crearVehiculo(color, esAmbulancia, listaParadas, cantidadDestinos);
}

void *threadReparacion(Reparacion *reparacion) {
	reparacionG(reparacion->casilla, reparacion->tiempoReparacion);
	printf("Nueva reparación: durará %d segundos\n", reparacion->tiempoReparacion);
	sleep(reparacion->tiempoReparacion);
	
	pthread_mutex_lock(&mutexThreaville);
	threadville[reparacion->casilla].estaOcupada = 0;
	pthread_mutex_unlock(&mutexThreaville);
	quitarReparacionG(reparacion->casilla);
	
	free(reparacion);
	printf("Reparación finalizada\n");
	pthread_exit(NULL);
}

/**
 * Handler para cancelar el thread de reparaciones.
 * @param arg
 */
static void desaparecerGestorRepa(void *arg) {
	printf("Se desaparecieron las reparaciones\n");
}

/**
 * Hilo gestor de reparaciones en Trheadville.
 * @param args
 * @return 
 */
void *threadReparaciones(void *args) {
	int proximaReparacion, mediaRep;
	pthread_cleanup_push(desaparecerGestorRepa, NULL);
	while (1) {
		pthread_testcancel();
		pthread_mutex_lock(&mutexParametros);
		mediaRep = mediaReparaciones;
		pthread_mutex_unlock(&mutexParametros);
		proximaReparacion = (int)((-1 * mediaRep)*log((double) 1 - ((double)rand() / ((double)(RAND_MAX)+(double)(1)))));
		printf("Proxima reparación en %d segundos\n", proximaReparacion);
		sleep(proximaReparacion);
		
		Reparacion *repa = (Reparacion *) malloc(sizeof(Reparacion));
		repa->tiempoReparacion = ((rand() % 3) + 1) * 5;
		do {
			repa->casilla = rand() % TOTAL_CASILLAS;
			pthread_mutex_lock(&mutexThreaville);
			if (!threadville[repa->casilla].estaOcupada) {
				threadville[repa->casilla].estaOcupada = 1;
				pthread_mutex_unlock(&mutexThreaville);
				break;
			}
			printf("Casilla ocupada, no se puede reparar\n");
			pthread_mutex_unlock(&mutexThreaville);
		} while (1);
		pthread_create(&(repa->threadReparacion), NULL, (void *) threadReparacion, (Reparacion *) repa);
	}
	pthread_cleanup_pop(1);
	pthread_exit(NULL);
}

/**
 * Comienza un thread para insertar una reparación en Threadville.
 */
void empezarReparaciones() {
	pthread_create(&generadorReparaciones, NULL, (void *) threadReparaciones, NULL);
}
/**
 * Mata el hilo de reparaciones.
 */
void pararReparaciones() {
	int rc = pthread_cancel(generadorReparaciones);
	if (!rc) {
		printf("Las reparaciones en Threadville han sido canceladas\n");
	}
}

/* PUENTES */

/**
 * Thread para gestionar los semáforos de los puentes Curly y Shemp.
 * @param args
 * @return 
 */
void *threadSemaforo(Puente *puente) {
	int tiempoEspera;
	threadville[puente->entradaN].semaforoVerde = 0; //Comienza poniendo en rojo la entrada norte
	while(1) {
		pthread_testcancel();
		pthread_mutex_lock(&mutexThreaville);
		threadville[puente->entradaN].semaforoVerde = !(threadville[puente->entradaN].semaforoVerde);
		threadville[puente->entradaS].semaforoVerde = !(threadville[puente->entradaS].semaforoVerde);
		pthread_mutex_unlock(&mutexThreaville);
		
		pintarSemaforo( puente->puenteID,
						threadville[puente->entradaN].semaforoVerde,
						threadville[puente->entradaS].semaforoVerde
		);
		
		pthread_mutex_lock(&mutexParametros);
		tiempoEspera = (puente->puenteID == P_CURLY) ? mCurly : nShemp;
		pthread_mutex_unlock(&mutexParametros);
		sleep(tiempoEspera);
	}
	pthread_exit(NULL);
}

/**
 * Thread para gestionar los semáforos de los puentes Larry y Joe.
 * @param args
 * @return 
 */
void *threadPolicia(Puente *puente) {
	int cuentaOficial = kLarryJoe;
	int ultimaCuenta = cuentaOficial;
	int entradaActual = puente->entradaN;
	int entradaContraria = puente->entradaS;
	threadville[puente->entradaN].cuentaOfiTran = cuentaOficial;
	threadville[puente->entradaS].cuentaOfiTran = 0;
	while(1) {
		pthread_testcancel();
		pthread_mutex_lock(&mutexThreaville);
		if (threadville[entradaActual].cuentaOfiTran == 0
				|| ((ultimaCuenta == threadville[entradaActual].cuentaOfiTran)
						&& (!threadville[entradaActual].arribo) && (threadville[entradaContraria].arribo))) {
			threadville[entradaActual].arribo = 0;
			threadville[entradaActual].cuentaOfiTran = 0;
			entradaContraria = entradaActual;
			entradaActual = (entradaActual == puente->entradaN) ? puente->entradaS : puente->entradaN;
			threadville[entradaActual].cuentaOfiTran = cuentaOficial;
		}
		threadville[entradaActual].arribo = 0;
		ultimaCuenta = threadville[entradaActual].cuentaOfiTran;
		pintarCuentaPuente( puente->puenteID, 
						threadville[puente->entradaN].cuentaOfiTran,
						threadville[puente->entradaS].cuentaOfiTran);
		pthread_mutex_unlock(&mutexThreaville);
		
		pthread_mutex_lock(&mutexParametros);
		cuentaOficial = kLarryJoe;
		pthread_mutex_unlock(&mutexParametros);
		usleep(500000);
	}
	pthread_exit(NULL);
}

/**
 * Establece los valores iniciales de las estructuras de los puentes y los hilos de gestión
 * (semáforos y oficiales de tránsito).
 */
void inicializarPuentes(){
	int p;
	for (p = 0; p <= P_JOE; p++) {
		puentes[p].puenteID = p;
		puentes[p].cuentaVehiculos = 0;
		puentes[p].direccion = 0;
		switch (p) {
			case P_LARRY:
				puentes[p].entradaN = 704;
				puentes[p].entradaS = 711;
				pthread_create(&(puentes[p].admin), NULL, (void *) threadPolicia, &puentes[p]);
				break;
			case P_CURLY:
				puentes[p].entradaN = 712;
				puentes[p].entradaS = 719;
				pthread_create(&(puentes[p].admin), NULL, (void *) threadSemaforo, &puentes[p]);
				break;
			case P_MOE:
				puentes[p].entradaN = 720;
				puentes[p].entradaS = 727;
				break;
			case P_SHEMP:
				puentes[p].entradaN = 728;
				puentes[p].entradaS = 735;
				pthread_create(&(puentes[p].admin), NULL, (void *) threadSemaforo, &puentes[p]);
				break;
			case P_JOE:
				puentes[p].entradaN = 736;
				puentes[p].entradaS = 743;
				pthread_create(&(puentes[p].admin), NULL, (void *) threadPolicia, &puentes[p]);
				break;
		}
	}
}

void pararAdminPuentes() {
	int p;
	for (p = 0; p <= P_JOE; p++) {
		if (p == P_MOE) { continue; }
		pthread_cancel(puentes[p].admin);
	}
}

/**
 * Actualiza los parámetros de la corrida de la ejecución.
 * @param k
 * @param m
 * @param n
 */
void actualizarParametros(ParamThreadville *parametros) {
	pthread_mutex_lock(&mutexParametros);
	kLarryJoe = parametros->k;
	mCurly = parametros->m;
	nShemp = parametros->fRepa;
	mediaReparaciones = parametros->fRepa;
	pthread_mutex_unlock(&mutexParametros);
	
	maxDestinos = parametros->maxDest;
	cantidadFija = parametros->fixedDest;
	sesgoRotondas = parametros->sesgoRot;
	
	free(parametros);
}

/**
 * Libera los recursos de memoria reservados y envía señales para terminal los hilos.
 */
void liberarRecursosEntidades() {
	// Aquí limpia todos los malloc y calloc realizados para los arreglos dinámicos, y esperará
	// por los hilos generadores, semáforos y tráficos para terminar.
	pararReparaciones();
	pararAdminPuentes();
	pthread_mutex_destroy(&mutexThreaville);
	pthread_mutex_destroy(&mutexParametros);
	pthread_mutex_destroy(&mutexRegistroVehicular);
}

/**
 * Inicializa las estructuras de manejo de entidades.
 */
void inicializarEntidades() {
	// Inicializar mutex
	pthread_mutex_init(&mutexThreaville, NULL);
	pthread_mutex_init(&mutexParametros, NULL);
	srand(time(NULL));
	generarBuses();
	inicializarPuentes();
}