/******************************************************************************
* ARCHIVO: entidades.h
* DESCRIPCIÓN:
* 	Archivo con la declaración de los métodos públicos de manejo gráfico de la
*		aplicación.
*   
* AUTORES:
* 	Mauricio Ramírez
* 	Paula Sánchez
* 	Johan Serrato
******************************************************************************/
#ifndef ENTIDADES_H
	#define ENTIDADES_H

/* PARÁMETROS GLOBALES */
/**
 * El número de autos K que cada deja pasar por cada lado de los puentes Larry y Joe
 */
int kLarryJoe;
/**
 * La cantidad de segundos M que dura el semáforo en el puente Curly.
 */
int mCurly;
/**
 * La cantidad de segundos N que dura el semáforo en el puente Shemp.
 */
int nShemp;

/**
 * Tiempo en segundos de la aparición de reparaciones en Threadville.
 */
int mediaReparaciones;

/**
 * Máximo de destinos para la generación automática de la ruta de un automóvil (sin contar origen Y
 * ni destino final Z).
 */
int maxDestinos;

/**
 * Indica si el máximo de destinos debe ser el número exacto de visitas que el carro generado debe
 * hacer.
 */
int cantidadFija;

/**
 * Factor que sirve para aumentar o disminuir la probabilidad de escoger las paradas Y y Z para la
 * generación de una ruta de visitas para un vehículo. su valor puede estar entre 1 y 2, en 2, las
 * probabilidades de escoger una parada Y o una parada Z aumentan en un 50%.
 */
double sesgoRotondas;

typedef struct
{
	int k;
	int m;
	int n;
	int fRepa;
	int maxDest;
	int fixedDest;
	double sesgoRot;
}ParamThreadville;

/* FUNCIONES ENTIDADES */
void generarBuses();
void crearBus(int);
void matarBus(int);

void generarVehiculo();
void generarAmbulancia();
void crearVehiculo(ColorAuto, int, char**, int);


void empezarReparaciones();
void pararReparaciones();

void liberarRecursosEntidades();
void inicializarEntidades();

void actualizarParametros(ParamThreadville *);

#endif /* ENTIDADES_H */