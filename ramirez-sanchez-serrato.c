/******************************************************************************
* ARCHIVO: ramirez-sanchez-serrato.c
* DESCRIPCIÓN:
* 	Proyecto 2 - Aventuras en Threadville
* 	Sistemas Operativos Avanzados
*   
* AUTORES:
* 	Mauricio Ramírez
* 	Paula Sánchez
* 	Johan Serrato
* ENTREGA: 02/06/2014
******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "defGlobal.h"
#include "gui/gui.h"
#include "entidades/entidades.h"

/* VARIABLES GLOBALES */
#define PESO_PISTA 1
#define PESO_NORMAL 50
int conteoCasillas = 0;

// Grafo principal
Casilla threadville[TOTAL_CASILLAS];
int pThreadville = 0; //Puntero para la creación de los nodos de Treadville.

// Matrices de Floyd
int arcosFloyd[TOTAL_CASILLAS][TOTAL_CASILLAS];
int caminosFloyd[TOTAL_CASILLAS][TOTAL_CASILLAS];

// Matriz de paradas. Incluye la parada de la rotonda Y y la de la rotonda Z.
int paradas[26][8];

/* PROTOTIPOS PRIVADOS */
void generarAutopista();
void generarCircunvalacion();
void generarTransversales();
void generarAvenidas();
void generarPuentes();

/* OPERACIONES PARA ESTRUCTURAS AUXILIARES */

/**
 * Inicializa matrices para el cálculo de los caminos óptimos.
 */
void inicializarFloyd() {
	int i, j;
	for(i = 0; i < TOTAL_CASILLAS; i++) {
		for(j = 0; j < TOTAL_CASILLAS; j++) {
			if (i != j) {
				arcosFloyd[i][j] = 32000;
			}
		}
	}
	
	for(i = 0; i < TOTAL_CASILLAS; i++) {
		for(j = 0; j < TOTAL_CASILLAS; j++) {
			caminosFloyd[i][j] = i;
		}
	}
}

/**
 * El algoritmo de Floyd para calcular la ruta más corta entre dos nodos cualquiera.
 */
void calcularCaminos() {
	int sParcial;
	int i, j, k;
	for (k = 0; k < TOTAL_CASILLAS; k++) {
		for (i = 0; i < TOTAL_CASILLAS; i++) {
			if (i != k) {
				for (j = 0; j < TOTAL_CASILLAS; j++) {
					if (j != k) {
						if (i == j) {
							arcosFloyd[i][j] = 0;
						} else {
							sParcial = arcosFloyd[i][k] + arcosFloyd[k][j];
							if (sParcial < arcosFloyd[i][j]) {
								caminosFloyd[i][j] = caminosFloyd[k][j];
								arcosFloyd[i][j] = sParcial;
							}
						}
					}
				}
			}
		}
	}
}

/**
 * Función para recuperar el camino más corto entre dos nodos del grafo de Threadville.
 * @param salida
 * @param destino
 * @return 
 */
int *obtenerRutaOptima(int salida, int destino) {
	int rutaTemp[TOTAL_CASILLAS];
	int pRuta = 0;
	int columnaFloyd = destino;
	rutaTemp[pRuta++] = destino;
	do {
		rutaTemp[pRuta++] = caminosFloyd[salida][columnaFloyd];
		columnaFloyd = caminosFloyd[salida][columnaFloyd];
	} while(columnaFloyd != salida);
	
	int* rutaOptima = malloc(pRuta * sizeof(int));
	//printf("Para ir de %d a %d:\n", salida, destino);
	int i, j;
	for(i = pRuta - 1, j = 0; i >= 0; i--, j++) {
		rutaOptima[j] = rutaTemp[i];
		//printf("->%d", rutaOptima[j]);
	}
	//printf("\n");
	
	return rutaOptima;
}

/**
 * Agrega la referencia a un nodo del grafo a la matriz de paradas para su identificación desde la 
 * parte gráfica.
 * @param idNodo
 * @param letraCuadra
 * @param numeroParada
 * @param paradaDerecha
 */
void agregarParada(int idNodo, const char letraCuadra, int numeroParada, int paradaDerecha) {
	int cuadra = ((int)letraCuadra) - 65;
	numeroParada--;
	paradas[cuadra][numeroParada] = idNodo;
	if (paradaDerecha) {
		snprintf(threadville[idNodo].etiquetaPD, 8, "%c%d", letraCuadra, numeroParada + 1);
		threadville[idNodo].etiquetaPI[0] = 0;
	}
	else {
		if (cuadra < 89) { //No es Y o Z
			snprintf(threadville[idNodo].etiquetaPI, 8, "%c", letraCuadra);
		}
		else {
			snprintf(threadville[idNodo].etiquetaPI, 8, "%c", letraCuadra);
		}
	}
}

/* OPERACIONES PARA CREAR ESTRUCTURAS BÁSICAS */

Casilla *crearCasilla(TipoCasilla tipo) {
	Casilla *casillaActual = &(threadville[pThreadville]);
	casillaActual->idCasilla = pThreadville++;
	casillaActual->tipoEspacio = tipo;
	casillaActual->estaOcupada = 0;
	if (tipo == INT_NORMAL) {
		//casillaActual->semaforoVerde = semaforoVerde;
		//casillaActual->cuentaOfiTran = 0;
	}
	return casillaActual;
}

void montarIntPuente(Casilla* intPuente, NombrePuente puente, int direccion, int semaforoVerde, int cuentaOT) {
	intPuente->nomPuente = puente;
	intPuente->dirEntrada = direccion;
	intPuente->semaforoVerde = semaforoVerde;
	intPuente->cuentaOfiTran = cuentaOT;
	intPuente->arribo = 0;
}

/**
 * Genera toda la estructura del grafo de Threadville y la información necesaria para calcular Floyd
 * y etiquetar las paradas.
 */
void generarThreadville() {
	generarAutopista();
	generarCircunvalacion();
	generarTransversales();
	generarAvenidas();
	generarPuentes();
}
/**
 * Genera las autopistas empezando por la rotonda Y.
 */
void generarAutopista() {
	int i;
	Casilla *casillaActual;
	Casilla *casillaAnterior = NULL;
	for(i = 0; i < 24; i++) {
		casillaActual = crearCasilla(NORMAL);
		if(casillaActual->idCasilla == 0) {
			agregarParada(0, 'Y', 1, 0);
			casillaActual->etiquetaPD[0] = 0;
		}
		
		if (casillaAnterior != NULL) {
			arcosFloyd[casillaAnterior->idCasilla][casillaActual->idCasilla] = PESO_PISTA;
		}
		casillaAnterior = casillaActual;
	}
	arcosFloyd[casillaAnterior->idCasilla][0] = PESO_PISTA;
	arcosFloyd[222][14] = PESO_PISTA;
	arcosFloyd[223][13] = PESO_PISTA;
	
	Casilla *casillaAntAfuera = &(threadville[9]);
	casillaAntAfuera->tipoEspacio = INT_ENT_PISTA;
	Casilla *casillaAntAdentro = &(threadville[10]);
	for(i = 0; i < 44; i++) {
		casillaActual = crearCasilla(AUTOPISTA);
		arcosFloyd[casillaAntAfuera->idCasilla][casillaActual->idCasilla] = PESO_PISTA;
		casillaAntAfuera = casillaActual;
		
		casillaActual = crearCasilla(AUTOPISTA);
		arcosFloyd[casillaAntAdentro->idCasilla][casillaActual->idCasilla] = PESO_PISTA;
		casillaAntAdentro = casillaActual;
	}
	
	casillaAnterior = NULL;
	for(i = 0; i < 24; i++) {
		casillaActual = crearCasilla(NORMAL);
		if(casillaActual->idCasilla == 135) {
			agregarParada(135, 'Z', 1, 0);
			casillaActual->etiquetaPD[0] = 0;
		}
		
		if (casillaAnterior != NULL) {
			arcosFloyd[casillaAnterior->idCasilla][casillaActual->idCasilla] = PESO_PISTA;
		}
		casillaAnterior = casillaActual;
	}
	arcosFloyd[135][112] = PESO_PISTA;
	arcosFloyd[110][125] = PESO_PISTA;
	arcosFloyd[111][124] = PESO_PISTA;
	
	casillaAntAfuera = &(threadville[120]);
	casillaAntAfuera->tipoEspacio = INT_ENT_PISTA;
	casillaAntAdentro = &(threadville[121]);
	for(i = 0; i < 44; i++) {
		casillaActual = crearCasilla(AUTOPISTA);
		arcosFloyd[casillaAntAfuera->idCasilla][casillaActual->idCasilla] = PESO_PISTA;
		casillaAntAfuera = casillaActual;
		
		casillaActual = crearCasilla(AUTOPISTA);
		arcosFloyd[casillaAntAdentro->idCasilla][casillaActual->idCasilla] = PESO_PISTA;
		casillaAntAdentro = casillaActual;
	}
}
/**
 * Genera circunvalación empezando y terminando desde la rotonda Y.
 */
void generarCircunvalacion() {
	Casilla *casillaActual;
	Casilla *casillaAnterior = &(threadville[16]);
	int i;
	for(i = 0; i < 148; i++) {
		casillaActual = crearCasilla(NORMAL);
		switch (i) {
			case 1:
				agregarParada(casillaActual->idCasilla, 'G', 5, 1);
				break;
			case 4:
				agregarParada(casillaActual->idCasilla, 'G', 6, 1);
				break;
			case 8:
				agregarParada(casillaActual->idCasilla, 'A', 7, 1);
				break;
			case 11:
				agregarParada(casillaActual->idCasilla, 'A', 8, 1);
				break;
			case 15:
				agregarParada(casillaActual->idCasilla, 'A', 1, 1);
				break;
			case 18:
				agregarParada(casillaActual->idCasilla, 'A', 2, 1);
				break;
			case 23:
				agregarParada(casillaActual->idCasilla, 'B', 1, 1);
				break;
			case 26:
				agregarParada(casillaActual->idCasilla, 'B', 2, 1);
				break;
			case 31:
				agregarParada(casillaActual->idCasilla, 'C', 1, 1);
				break;
			case 34:
				agregarParada(casillaActual->idCasilla, 'C', 2, 1);
				break;
			case 39:
				agregarParada(casillaActual->idCasilla, 'D', 1, 1);
				break;
			case 42:
				agregarParada(casillaActual->idCasilla, 'D', 2, 1);
				break;
			case 47:
				agregarParada(casillaActual->idCasilla, 'E', 1, 1);
				break;
			case 50:
				agregarParada(casillaActual->idCasilla, 'E', 2, 1);
				break;
			case 55:
				agregarParada(casillaActual->idCasilla, 'F', 1, 1);
				break;
			case 58:
				agregarParada(casillaActual->idCasilla, 'F', 2, 1);
				break;
			case 62:
				agregarParada(casillaActual->idCasilla, 'F', 3, 1);
				break;
			case 65:
				agregarParada(casillaActual->idCasilla, 'F', 4, 1);
				break;
			case 69:
				agregarParada(casillaActual->idCasilla, 'L', 3, 1);
				break;
			case 72:
				agregarParada(casillaActual->idCasilla, 'L', 4, 1);
				break;
			case 75:
				agregarParada(casillaActual->idCasilla, 'R', 2, 1);
				break;
			case 78:
				agregarParada(casillaActual->idCasilla, 'R', 3, 1);
				break;
			case 82:
				agregarParada(casillaActual->idCasilla, 'X', 3, 1);
				break;
			case 85:
				agregarParada(casillaActual->idCasilla, 'X', 4, 1);
				break;
			case 89:
				agregarParada(casillaActual->idCasilla, 'X', 5, 1);
				break;
			case 92:
				agregarParada(casillaActual->idCasilla, 'X', 6, 1);
				break;
			case 97:
				agregarParada(casillaActual->idCasilla, 'W', 5, 1);
				break;
			case 100:
				agregarParada(casillaActual->idCasilla, 'W', 6, 1);
				break;
			case 105:
				agregarParada(casillaActual->idCasilla, 'V', 5, 1);
				break;
			case 108:
				agregarParada(casillaActual->idCasilla, 'V', 6, 1);
				break;
			case 113:
				agregarParada(casillaActual->idCasilla, 'U', 5, 1);
				break;
			case 116:
				agregarParada(casillaActual->idCasilla, 'U', 6, 1);
				break;
			case 121:
				agregarParada(casillaActual->idCasilla, 'T', 5, 1);
				break;
			case 124:
				agregarParada(casillaActual->idCasilla, 'T', 6, 1);
				break;
			case 129:
				agregarParada(casillaActual->idCasilla, 'S', 5, 1);
				break;
			case 132:
				agregarParada(casillaActual->idCasilla, 'S', 6, 1);
				break;
			case 136:
				agregarParada(casillaActual->idCasilla, 'S', 7, 1);
				break;
			case 139:
				agregarParada(casillaActual->idCasilla, 'S', 8, 1);
				break;
			case 143:
				agregarParada(casillaActual->idCasilla, 'M', 6, 1);
				break;
			case 146:
				agregarParada(casillaActual->idCasilla, 'M', 1, 1);
				break;
		}
		
		arcosFloyd[casillaAnterior->idCasilla][casillaActual->idCasilla] = PESO_NORMAL;
		if (casillaActual->idCasilla == 297) {
			arcosFloyd[297][118] = PESO_PISTA;
			casillaAnterior = &(threadville[127]);
			continue;
		}
		casillaAnterior = casillaActual;
	}
	arcosFloyd[casillaAnterior->idCasilla][7] = PESO_PISTA;
}
/**
 * Genera las dos transversales de Theadville desde los cruces generados por la circunvalación.
 */
void generarTransversales() {
	Casilla *casillaActual;
	Casilla *casillaAnterior = &(threadville[291]);
	int i;
	int parTra = 0;
	int cuadraDer = 70;
	int cuadraIzq = 76;
	for(i = 0; i < 46; i++) {
		casillaActual = crearCasilla(NORMAL);
		switch(parTra) {
			case 1:
				agregarParada(casillaActual->idCasilla, (char)cuadraDer, 5, 1);
				agregarParada(casillaActual->idCasilla, (char)cuadraIzq, 2, 0);
				break;
			case 4:
				agregarParada(casillaActual->idCasilla, (char)cuadraDer, 6, 1);
				agregarParada(casillaActual->idCasilla, (char)cuadraIzq, 1, 0);
				cuadraDer--;
				cuadraIzq--;
				parTra = -4;
				break;
		}
		parTra++;
		
		arcosFloyd[casillaAnterior->idCasilla][casillaActual->idCasilla] = PESO_NORMAL;
		casillaAnterior = casillaActual;
	}
	arcosFloyd[casillaAnterior->idCasilla][230] = PESO_NORMAL;
	
	parTra = 0;
	cuadraDer = 83;
	cuadraIzq = 77;
	casillaAnterior = &(threadville[365]);
	for(i = 0; i < 46; i++) {
		casillaActual = crearCasilla(NORMAL);
		switch(parTra) {
			case 1:
				agregarParada(casillaActual->idCasilla, (char)cuadraDer, 1, 1);
				agregarParada(casillaActual->idCasilla, (char)cuadraIzq, 5, 0);
				break;
			case 4:
				agregarParada(casillaActual->idCasilla, (char)cuadraDer, 2, 1);
				agregarParada(casillaActual->idCasilla, (char)cuadraIzq, 4, 0);
				cuadraDer++;
				cuadraIzq++;
				parTra = -4;
				break;
		}
		parTra++;
		
		arcosFloyd[casillaAnterior->idCasilla][casillaActual->idCasilla] = PESO_NORMAL;
		casillaAnterior = casillaActual;
	}
	arcosFloyd[casillaAnterior->idCasilla][304] = PESO_NORMAL;
}
/**
 * Genera las avenidas de Treadville a partir de los cruces generados por la circunvalación y
 * tomando en cuenta los generados por las transversales.
 */
void generarAvenidas() {
	int cruces[] = {
		244, 411, 704, 410, 245,
		252, 403, 712, 402, 253,
		260, 395, 720, 394, 261,
		268, 387, 728, 386, 269,
		276, 379, 736, 378, 277,
		
		318, 457, 743, 456, 319,
		326, 449, 735, 448, 327,
		334, 441, 727, 440, 335,
		342, 433, 719, 432, 343,
		350, 425, 711, 424, 351
	};
	int pCruces = 0;
	Casilla *casillaActual;
	Casilla *casillaAnterior = &(threadville[cruces[pCruces++]]);
	int i;
	int parTra = 0;
	int cuadraPri = 65;
	int cuadraSec = 71;
	for(i = 0; i < 120; i++) {
		casillaActual = crearCasilla(NORMAL);
		switch(parTra) {
			case 1:
				agregarParada(casillaActual->idCasilla, (char)cuadraPri, 3, 1);
				break;
			case 4:
				agregarParada(casillaActual->idCasilla, (char)cuadraPri, 4, 1);
				cuadraPri++;
				break;
			case 7:
				agregarParada(casillaActual->idCasilla, (char)cuadraSec, 3, 1);
				break;
			case 10:
				agregarParada(casillaActual->idCasilla, (char)cuadraSec, 4, 1);
				cuadraSec++;
				break;
			case 13:
				agregarParada(casillaActual->idCasilla, (char)cuadraSec, 5, 1);
				break;
			case 16:
				agregarParada(casillaActual->idCasilla, (char)cuadraSec, 6, 1);
				break;
			case 19:
				agregarParada(casillaActual->idCasilla, (char)cuadraPri, 7, 1);
				break;
			case 22:
				agregarParada(casillaActual->idCasilla, (char)cuadraPri, 8, 1);
				parTra = -2;
				break;
		}
		parTra++;
		
		arcosFloyd[casillaAnterior->idCasilla][casillaActual->idCasilla] = PESO_NORMAL;
		casillaAnterior = casillaActual;
		
		if (parTra%6 == 0) {
			//printf("Entró con parTra = %d | pCruces = %d\n", parTra, pCruces);
			arcosFloyd[casillaAnterior->idCasilla][cruces[pCruces]] = PESO_NORMAL;
			if (parTra > 0) {
				casillaAnterior = &(threadville[cruces[pCruces]]);
				if(casillaAnterior->idCasilla == 0) {
					casillaAnterior->idCasilla = cruces[pCruces];
				}
			}
			else {
				casillaAnterior = &(threadville[cruces[++pCruces]]);
			}
			pCruces++;
		}
	}
	
	parTra = 0;
	cuadraPri = 88;
	cuadraSec = 82;
	for(i = 0; i < 120; i++) {
		casillaActual = crearCasilla(NORMAL);
		switch(parTra) {
			case 1:
				agregarParada(casillaActual->idCasilla, (char)cuadraPri, 7, 1);
				break;
			case 4:
				agregarParada(casillaActual->idCasilla, (char)cuadraPri, 8, 1);
				cuadraPri--;
				break;
			case 7:
				agregarParada(casillaActual->idCasilla, (char)cuadraSec, 6, 1);
				break;
			case 10:
				agregarParada(casillaActual->idCasilla, (char)cuadraSec, 1, 1);
				cuadraSec--;
				break;
			case 13:
				agregarParada(casillaActual->idCasilla, (char)cuadraSec, 2, 1);
				break;
			case 16:
				agregarParada(casillaActual->idCasilla, (char)cuadraSec, 3, 1);
				break;
			case 19:
				agregarParada(casillaActual->idCasilla, (char)cuadraPri, 3, 1);
				break;
			case 22:
				agregarParada(casillaActual->idCasilla, (char)cuadraPri, 4, 1);
				parTra = -2;
				break;
		}
		parTra++;
		
		arcosFloyd[casillaAnterior->idCasilla][casillaActual->idCasilla] = PESO_NORMAL;
		casillaAnterior = casillaActual;
		
		if (parTra%6 == 0) {
			arcosFloyd[casillaAnterior->idCasilla][cruces[pCruces]] = PESO_NORMAL;
			if (parTra > 0) {
				casillaAnterior = &(threadville[cruces[pCruces]]);
				if(casillaAnterior->idCasilla == 0) {
					casillaAnterior->idCasilla = cruces[pCruces];
				}
			}
			else {
				pCruces++;
				if (pCruces < 50) {
					casillaAnterior = &(threadville[cruces[pCruces]]);
				}
			}
			pCruces++;
		}
	}
	
/*
	int salida, destino;
	for(salida = 0; salida <= 1; salida++) {
		printf("El nodo %d se conecta a: ", salida);
		for(destino = 0; destino < TOTAL_CASILLAS; destino++) {
			if (salida != destino && (arcosFloyd[salida][destino] < 32000)) {
				printf("|%d ", destino);
			}
		}
		printf(".\n\n");
	}
*/
}
/**
 * Genera los puentes a partir de los cruces generados por las avenidas.
 */
void generarPuentes() {
	int crucesPuentes[] = { 476, 692, 500, 668, 524, 644, 548, 620, 572, 596 };
	int pCrucesP = 0;
	Casilla *casillaActual;
	Casilla *casillaAnterior;
	int i, modTo8;
	int puenteActual = 0;
	for(i = 0; i < 40; i++) {
		modTo8 = i % 8;
		if (modTo8 == 0 || modTo8 == 7) {
			casillaActual = crearCasilla(INT_PUENTE);
			arcosFloyd[casillaActual->idCasilla][crucesPuentes[pCrucesP++]] = PESO_NORMAL;
			if (modTo8 == 7) {
				montarIntPuente(casillaActual, puenteActual++, 1, 1, -1);
				arcosFloyd[casillaAnterior->idCasilla][casillaActual->idCasilla] = PESO_NORMAL;
				arcosFloyd[casillaActual->idCasilla][casillaAnterior->idCasilla] = PESO_NORMAL;
			}
			else {
				montarIntPuente(casillaActual, puenteActual, 0, 1, -1);
			}
		}
		else {
			casillaActual = crearCasilla(PUENTE);
			casillaActual->nomPuente = puenteActual;
			arcosFloyd[casillaAnterior->idCasilla][casillaActual->idCasilla] = PESO_NORMAL;
			arcosFloyd[casillaActual->idCasilla][casillaAnterior->idCasilla] = PESO_NORMAL;
		}
		casillaAnterior = casillaActual;
	}
}

/**
 * Etiqueta todas las intersecciones de Threadville.
 */
void identificarIntersecciones() {
	int intersecciones[] = {
		7,13,14,118,124,125,
		230,244,245,252,253,260,261,268,269,276,277,291,304,318,319,326,327,334,335,342,343,350,351,365,
		378,379,386,387,394,395,402,403,410,411,424,425,432,433,440,441,448,449,456,457
	};
	int totalIntersecciones = 50;
	int i;
	for(i = 0; i < totalIntersecciones; i++) {
		threadville[intersecciones[i]].tipoEspacio = INT_NORMAL;
	}
}

int main(int argc, char *argv[])
{
	printf("Tarea programada #2\nIntegrantes:\nMauricio Ramírez - Paula Sánchez - Johan Serrato\n--\n\n");
	
	inicializarFloyd();
	printf("Generando estructuras lógicas...\n");
	generarThreadville();
	identificarIntersecciones();
	printf("Calculando los caminos más cortos...\n");
	calcularCaminos();
	
	//int *pruebaCamino = obtenerRutaOptima(0, 135);
	//free(pruebaCamino);
/*
	// Para comprobar matriz de paradas
	int i, j;
	char cuadra;
	for(i = 0; i < 26; i++) {
		cuadra = (char)(i + 65);
		for(j = 0; j < 8; j++) {
			if(paradas[i][j] != 0) {
				printf("%c%d: %d\n", cuadra, j + 1, paradas[i][j]);
			}
		}
		printf("\n");
	}
*/
	
	inicializarEntidades();
	
	/* Inicializar GUI */
	inicializarGUI(&argc, &argv);
	// Cuando la gráfica esté lista, llamará a una función lógica para arrancar todos los hilos de
	// puentes (semáforos, tráficos), buses, e hilos de generación automática de reparaciones.
	
	printf("Finalización exitosa\n");
	return (0);
}
