CFLAGS=-Igui
GTKFLAGS=`pkg-config --libs --cflags gtk+-3.0`
PTFLAGS=-pthread
LMFLAG=-lm
GDBFLAG=-g
OBJETOS=ramirez-sanchez-serrato.o gui/gui.o entidades/entidades.o

proyecto2: $(OBJETOS)
	gcc -Wall -o ramirez-sanchez-serrato $(OBJETOS) $(LMFLAG) $(GTKFLAGS)
	
gui/gui.o: gui/gui.c gui/gui.h defGlobal.h entidades/entidades.h
	gcc -Wall -c gui/gui.c -o gui/gui.o $(GTKFLAGS)
	
entidades/entidades.o: entidades/entidades.c entidades/entidades.h defGlobal.h gui/gui.h
	gcc -Wall -c entidades/entidades.c -o entidades/entidades.o $(LMFLAG) $(PTFLAGS)

ramirez-sanchez-serrato.o: ramirez-sanchez-serrato.c defGlobal.h gui/gui.h
	gcc -Wall -c ramirez-sanchez-serrato.c -o ramirez-sanchez-serrato.o $(GTKFLAGS)

#FUENTES=ramirez-sanchez-serrato.c gui/gui.c

#proyecto2: $(OBJETOS)
#	gcc -Wall -o ramirez-sanchez-serrato $(OBJETOS) `pkg-config --libs --cflags gtk+-3.0`

# ~$ make depend
#depend:
#	makedepend $(CFLAGS) $(FUENTES)
